package core;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月15日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class BioHandler {
	private int port;
	private ServerSocket serverSocket;
	private ExecutorService executors;

	public BioHandler(int port) {
		this.port = port;
		//TODO 参数修改
		executors = Executors.newFixedThreadPool(10);

	}


	public static void main(String[] args) {
		new BioHandler(8899).start();
	}

	private void start() {
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("启动成功，端口为" + port);
			while (true) {
				Socket client = serverSocket.accept();
				executors.execute(new ThreadLoop(client));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}