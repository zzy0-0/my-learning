package core;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月24日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class Test {

	void fun(int i) {
		i = 2;
	}


	public static void main(String[] args) {
		Test test = new Test();
		int i = 10;
		test.fun(10);
		System.out.println(i);
	}
}