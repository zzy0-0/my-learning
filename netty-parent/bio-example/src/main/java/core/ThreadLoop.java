package core;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月15日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class ThreadLoop implements Runnable {
	Socket client;

	public ThreadLoop(Socket client) {
		this.client = client;
	}

	private void process(Socket client) throws Exception {
		InputStream is = client.getInputStream();
		OutputStream os = client.getOutputStream();

		MyRequest myRequest = new MyRequest(is);

		new MyResponse(os).write("hell world");

		os.flush();

		os.close();

		is.close();
		client.close();
	}


	@Override
	public void run() {
		try {
			process(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}