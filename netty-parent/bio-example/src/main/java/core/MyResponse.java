package core;

import java.io.OutputStream;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月15日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MyResponse {
	private OutputStream out;

	public MyResponse(OutputStream os) {
		this.out = os;
	}

	public void write(String s) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("HTTP/1.1 200 ok\n")
				.append("Content-Type: text/html;\n")
				.append("\r\n")
				.append(s);
		System.out.println("写消息：" + sb.toString());
		out.write(sb.toString().getBytes());
	}

}