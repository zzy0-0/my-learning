package core;

import java.io.IOException;
import java.io.InputStream;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月15日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MyRequest {
	private String content;

	public MyRequest(InputStream is) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();

		byte[] buff = new byte[1024];
		int len = 0;
	/*	while ((len = is.read(buff)) > 0) {
			stringBuilder.append(new String(buff, 0, len));
		}*/

		if ((len = is.read(buff)) > 0) {
			stringBuilder.append(new String(buff, 0, len));
		}
		System.out.println("收到消息 ： " + stringBuilder.toString());
		content = stringBuilder.toString();

	}

	public String getContent() {
		return content;
	}
}