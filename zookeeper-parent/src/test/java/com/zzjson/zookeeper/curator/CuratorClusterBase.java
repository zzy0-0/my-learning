package com.zzjson.zookeeper.curator;


public class CuratorClusterBase extends CuratorStandaloneBase {

	private final static String CLUSTER_CONNECT_STR = "my-server:2181,my-server:2182,my-server:2183,my-server:2184";

	public String getConnectStr() {
		return CLUSTER_CONNECT_STR;
	}
}
