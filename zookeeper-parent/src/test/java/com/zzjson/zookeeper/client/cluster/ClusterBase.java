package com.zzjson.zookeeper.client.cluster;

import com.zzjson.zookeeper.client.StandaloneBase;

public class ClusterBase extends StandaloneBase {

    private final static  String CLUSTER_CONNECT_STR="my-server:2181,my-server:2182,my-server:2183,my-server:2184";


    private static final  int CLUSTER_SESSION_TIMEOUT=60 * 1000;


    @Override
    protected String getConnectStr() {
        return CLUSTER_CONNECT_STR;
    }

    @Override
    protected int getSessionTimeout() {
        return CLUSTER_SESSION_TIMEOUT;
    }
}
