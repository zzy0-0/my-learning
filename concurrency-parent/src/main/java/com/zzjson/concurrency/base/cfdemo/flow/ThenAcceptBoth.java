package com.zzjson.concurrency.base.cfdemo.flow;


import com.zzjson.concurrency.tools.SleepTools;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：结合消费类
 *
 * 需要上一步的处理返回值，并且 other 代表的 CompletionStage 有返回值之 后，利用这两个返回值，进行消费
 *
 *
 */
public class ThenAcceptBoth {
    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> {
            SleepTools.second(1);
            return "hello";
        }).thenAcceptBoth(
            CompletableFuture.supplyAsync(() -> {
            SleepTools.second(2);
            return "world";
        }), (s1, s2) -> System.out.println(s1 + " " + s2));
        SleepTools.second(3);
    }
}
