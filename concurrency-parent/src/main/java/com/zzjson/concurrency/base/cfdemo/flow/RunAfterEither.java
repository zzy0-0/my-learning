package com.zzjson.concurrency.base.cfdemo.flow;


import com.zzjson.concurrency.tools.SleepTools;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：取最快运行后执行类
 *  两个 CompletionStage，谁计算的快，我就用那个 CompletionStage 的结果进 行下一步的转化操作。现实开发场景中，总会碰到有两种渠道完成同一个事情， 所以就可以调用这个方法，找一个最快的结果进行处理。
 */
public class RunAfterEither {
    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> {
            SleepTools.second(2);
            return "s1";
        }).runAfterEither(CompletableFuture.supplyAsync(() -> {
            SleepTools.second(1);
            return "s2";
        }), () -> System.out.println("hello world"));
        SleepTools.second(3);
    }
}
