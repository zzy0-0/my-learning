package com.zzjson.concurrency.base.cfdemo.flow;

import com.zzjson.concurrency.tools.SleepTools;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：取最快消费类
 * 两个 CompletionStage，谁计算的快，我就用那个 CompletionStage 的结果进 行下一步的消费操作。
 *
 */
public class AcceptEither {
    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> {
            SleepTools.second(1);
            return "s1";
        }).acceptEither(CompletableFuture.supplyAsync(() -> {
            SleepTools.second(2);
            return "hello world";
        }), System.out::println);
        SleepTools.second(3);
    }
}
