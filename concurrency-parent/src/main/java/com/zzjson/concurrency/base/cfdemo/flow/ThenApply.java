package com.zzjson.concurrency.base.cfdemo.flow;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：变换类
 *
 * 关键入参是函数式接口 Function。它的入参是上一个阶段计算后的结果，返回值是经过转化后结果。
 *
 */
public class ThenApply {
    public static void main(String[] args) {
        String result = CompletableFuture.supplyAsync(() -> "hello")
                .thenApply(s -> s + " world").join();
        System.out.println(result);
    }
}
