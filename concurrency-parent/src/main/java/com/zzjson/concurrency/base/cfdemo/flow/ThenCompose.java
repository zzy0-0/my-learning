package com.zzjson.concurrency.base.cfdemo.flow;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：结合转化类
 *
 * 对于 Compose 可以连接两个 CompletableFuture，其内部处理逻辑是当第一 个 CompletableFuture 处理没有完成时会合并成一个 CompletableFuture,如果处理 完成，第二个
 * future 会紧接上一个 CompletableFuture 进行处理。
 *  第一个 CompletableFuture 的处理结果是第二个 future 需要的输入参数。
 *
 *
 */
public class ThenCompose {
    public static void main(String[] args) {
        Integer result =
            CompletableFuture.supplyAsync(() -> 10)
                .thenCompose(i -> CompletableFuture.supplyAsync(() -> i + 1))
                .join();
        System.out.println(result);
    }
}
