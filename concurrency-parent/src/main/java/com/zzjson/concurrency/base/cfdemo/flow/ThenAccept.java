package com.zzjson.concurrency.base.cfdemo.flow;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：消费类
 *  关键入参是函数式接口 Consumer。它的入参是上一个阶段计算后的结果， 没有返回值。
 */
public class ThenAccept {
    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> "hello")
                .thenAccept(s -> System.out.println(s+" world"));
    }
}
