package com.zzjson.concurrency.base.cfdemo.flow;


import com.zzjson.concurrency.tools.SleepTools;

import java.util.concurrent.CompletableFuture;

/**
 * 类说明：取最快转换类
 *  两个 CompletionStage，谁计算的快，我就用那个 CompletionStage 的结果进 行下一步的转化操作。现实开发场景中，总会碰到有两种渠道完成同一个事情， 所以就可以调用这个方法，找一个最快的结果进行处理。
 */
public class ApplyToEither {
    public static void main(String[] args) {
        String result = CompletableFuture.supplyAsync(() -> {
            SleepTools.second(1);
            return "s1 耗时1秒";
        }).applyToEither(CompletableFuture.supplyAsync(() -> {
            SleepTools.second(2);
            return "s2 耗时2秒";
        }), s -> s).join();
        System.out.println(result);
    }
}
