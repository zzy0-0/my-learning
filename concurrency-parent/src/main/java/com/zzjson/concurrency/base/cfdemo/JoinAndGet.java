package com.zzjson.concurrency.base.cfdemo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 类说明：get和join方法的区别
 */
public class JoinAndGet {
    public static void main(String[] args)
            throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(()->{
           int i = 1/0;
           return 100;
        });

        //Exception in thread "main" java.util.concurrent.ExecutionException: java.lang.ArithmeticException: / by zero
        //	at java.util.concurrent.CompletableFuture.reportGet(CompletableFuture.java:357)
        //	at java.util.concurrent.CompletableFuture.get(CompletableFuture.java:1908)
        //	at com.zzjson.concurrency.base.cfdemo.JoinAndGet.main(JoinAndGet.java:16)
        //Caused by: java.lang.ArithmeticException: / by zero
        //	at com.zzjson.concurrency.base.cfdemo.JoinAndGet.lambda$main$0(JoinAndGet.java:13)
        //	at java.util.concurrent.CompletableFuture$AsyncSupply.run(CompletableFuture.java:1604)
        //	at java.util.concurrent.CompletableFuture$AsyncSupply.exec(CompletableFuture.java:1596)
        //	at java.util.concurrent.ForkJoinTask.doExec(ForkJoinTask.java:289)
        //	at java.util.concurrent.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:1056)
        //	at java.util.concurrent.ForkJoinPool.runWorker(ForkJoinPool.java:1692)
        //	at java.util.concurrent.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:157)
        //future.get();



        //join 返回计算的结果或者抛出一个 unchecked 异常(CompletionException)，它 和 get 对抛出的异常的处理有些细微的区别。

        //Exception in thread "main" java.util.concurrent.CompletionException: java.lang.ArithmeticException: / by zero
        //	at java.util.concurrent.CompletableFuture.encodeThrowable(CompletableFuture.java:273)
        //	at java.util.concurrent.CompletableFuture.completeThrowable(CompletableFuture.java:280)
        //	at java.util.concurrent.CompletableFuture$AsyncSupply.run(CompletableFuture.java:1606)
        //	at java.util.concurrent.CompletableFuture$AsyncSupply.exec(CompletableFuture.java:1596)
        //	at java.util.concurrent.ForkJoinTask.doExec(ForkJoinTask.java:289)
        //	at java.util.concurrent.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:1056)
        //	at java.util.concurrent.ForkJoinPool.runWorker(ForkJoinPool.java:1692)
        //	at java.util.concurrent.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:157)
        //Caused by: java.lang.ArithmeticException: / by zero
        //	at com.zzjson.concurrency.base.cfdemo.JoinAndGet.lambda$main$0(JoinAndGet.java:13)
        //	at java.util.concurrent.CompletableFuture$AsyncSupply.run(CompletableFuture.java:1604)
        //	... 5 more
        future.join();
    }
}
