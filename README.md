# 黄金时代的架构之路

## 一、项目介绍
这是黄金时代的架构之路的笔记&代码库，大家可以通过拉取项目进行学习交流

## 二、结构介绍

| 父模块          | 模块                         | 模块介绍                                                     | 备注 |
| --------------- | ---------------------------- | ------------------------------------------------------------ | ---- |
| my-bom          | my-bom                       | 项目的整体依赖管理模块，依赖都定义在当前包                   |      |
| cloud-parent    |                              | 微服务项目演示代码                                           |      |
|                 | my-alibaba-dao               | 当前模块提供了微服务的基础dao支持                            |      |
|                 | nacos-parent                 | nacos 的客户端模块  订单和产品模块，提供了一些基础的使用     |      |
|                 | ribbon-parent                | ribbon的基础演示                                             |      |
|                 | ribbon-order                 | 把支付和产品发布到nacos，order尝试基于RestTeamplte原生调用   |      |
| designPatterns  |                              | (2020年12月29日 - 设计模式基本原则)                          |      |
|                 |                              | factory (2020年12月30日 - 当前包含简单工厂，工厂方法，抽象工厂) |      |
| my-common       |                              | 一些公共的配置信息                                           |      |
| redis-parent    |                              | redis的演示代码                                              |      |
|                 | redis-jedis                  | 基于jedis的 redis 客户端基础使用                             |      |
|                 | redis-redission              | 基于redission的 redis 客户端基础使用                         |      |
|                 | redis-springboot             | redis 和 SpringBoot 整合的应用                               |      |
| spring-parent   |                              |                                                              |      |
|                 | spring-aop                   | AOP 的实例代码包括XML 和注解                                 |      |
|                 | spring-boot-example          | Spring boot 应用基本示例                                     |      |
|                 | spring-circular-dependencies | Spring 循环依赖示例                                          |      |
|                 | spring-dynamic-datasource    | 动态数据源的演示代码                                         |      |
|                 | spring-freemarker            | Spring 与 freemarker 的整合                                  |      |
| leetcode-parent |                              | leetcode 刷题的代码                                          |      |
|                 | array                        | https://www.yuque.com/huangjinshidai-1wt9t/ocmvo8/ah3bpwgnpe793s4t?singleDoc# 《001-数组、链表、跳表的基本实现和特性》 |      |
|                 | recursion                    | https://www.yuque.com/huangjinshidai-1wt9t/ocmvo8/cl12bigigbd6pq5q?singleDoc# 《泛型递归、树的递归》 |      |



## 三、笔记大纲

本项目对应了自己的个人笔记，每一个写入的项目都会贴入个人的笔记和思考

<img src="https://xiaochi-pic.oss-cn-hangzhou.aliyuncs.com/markdown/20231010115126.png" style="zoom:33%;" />

## 四、参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

