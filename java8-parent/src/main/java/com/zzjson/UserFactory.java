package com.zzjson;

import java.util.Arrays;
import java.util.List;

/**
 * @Author 霄池
 * @Date 2022/5/25 10:15 上午
 */
public class UserFactory {
    public static List<User> create() {
        User user1 = User.builder()
            .name("张1")
            .id(1L)
            .age(1)
            .build();

        User user2 = User.builder()
            .name("张2")
            .id(2L)
            .age(2)
            .build();

        User user3 = User.builder()
            .name("张3")
            .id(3L)
            .age(3)
            .build();

        User user4 = User.builder()
            .name("张4")
            .id(4L)
            .age(4)
            .build();

        User user5 = User.builder()
            .name("张5")
            .id(1L)
            .age(5)
            .build();

        return Arrays.asList(user2, user1, user3, user4, user5);

    }
}
