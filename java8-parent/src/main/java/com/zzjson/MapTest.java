package com.zzjson;

import java.util.List;

/**
 * @Author 霄池
 * @Date 2022/5/25 10:14 上午
 */
public class MapTest {
    public static void main(String[] args) {
        List<User> users = UserFactory.create();
        users.sort((o1, o2) -> {
            return o1.getId().compareTo(o2.getId());
        });

    /*    Map<Long, String> collect =
            users.stream().filter(Objects::nonNull).collect(Collectors.toMap(User::getId, User::getName));

*/
        System.out.println(users);
    }

}
