package com.zzjson.redisspringboot;

public interface ResultCodeInterface {
    /**
     * 错误码
     *
     * @return 错误码
     */
    String code();

    /**
     * 错误具体信息
     *
     * @return 错误具体信息
     */
    String message();
}