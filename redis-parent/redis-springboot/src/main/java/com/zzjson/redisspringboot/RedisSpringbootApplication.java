package com.zzjson.redisspringboot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

@SpringBootApplication
@Slf4j
public class RedisSpringbootApplication implements ApplicationRunner {
    @Resource
    private RedisTemplate redisTemplate;

    public static void main(String[] args) {
        SpringApplication.run(RedisSpringbootApplication.class, args);

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        while (Boolean.TRUE) {

            redisTemplate.boundValueOps("zs").set("1");

            Object zs = redisTemplate.boundValueOps("zs").get();
            log.error("#######{}", zs);
        }

    }
}
