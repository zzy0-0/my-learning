package com.zzjson.jvm;

import org.openjdk.jol.info.ClassLayout;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class T0_BasicLock {
	public static void main(String[] args) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Object o = new Object();
		System.out.println(ClassLayout.parseInstance(o).toPrintable());

		new Thread(() -> {
			synchronized (o) {
				System.out.println(ClassLayout.parseInstance(o).toPrintable());
			}
		}).start();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(ClassLayout.parseInstance(o).toPrintable());
		new Thread(() -> {
			synchronized (o) {
				System.out.println(ClassLayout.parseInstance(o).toPrintable());
			}
		}).start();
	}
}