package com.zzjson.jvm;

import org.openjdk.jol.info.ClassLayout;

import java.util.stream.Stream;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class JOLSample {

	public static void main(String[] args) {
		Object o = new Object();
		String s = "d1";
		Stream<String> s1 = Stream.of(s);
		synchronized (o) {
			ClassLayout layout = ClassLayout.parseInstance(o);
			System.out.println(layout.toPrintable());
		}


		System.out.println();
		ClassLayout layout1 = ClassLayout.parseInstance(new int[]{});
		System.out.println(layout1.toPrintable());

		System.out.println();
		ClassLayout layout2 = ClassLayout.parseInstance(new A());
		System.out.println(layout2.toPrintable());
	}

	// -XX:+UseCompressedOops           默认开启的压缩所有指针
	// -XX:+UseCompressedClassPointers  默认开启的压缩对象头里的类型指针Klass Pointer
	// Oops : Ordinary Object Pointers
	public static class A {
		//8B mark word
		//4B Klass Pointer   如果关闭压缩-XX:-UseCompressedClassPointers或-XX:-UseCompressedOops，则占用8B
		int id;        //4B
		String name;   //4B  如果关闭压缩-XX:-UseCompressedOops，则占用8B
		byte b;        //1B
		Object o;      //4B  如果关闭压缩-XX:-UseCompressedOops，则占用8B
	}
}