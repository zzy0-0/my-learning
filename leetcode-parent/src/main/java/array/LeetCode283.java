package array;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * 移动零到最后： https://leetcode.cn/problems/move-zeroes/
 * @Author 霄池
 * @Date 2023/10/9 23:47
 */

@RunWith(JUnit4.class)
public class LeetCode283 {

    @Test
    public void tests() {
        int[] ints = {0, 1, 0, 3, 12};
        int[] ints1 = moveZeroesByArray(ints);
        Assert.assertArrayEquals(ints1, new int[] {1, 3, 12, 0, 0});
    }

    public int[] moveZeroesByArray(int[] nums) {
        int balls = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                balls++;
                continue;
            }
            if (balls > 0) {
                int temp = nums[i];
                nums[i] = 0;
                nums[i - balls] = temp;
            }
        }
        return nums;
    }

    @Test
    public void test2() {
        int[] ints = {0, 1, 0, 3, 12};
        int[] ints1 = moveZeroesByPoint(ints);
        Assert.assertArrayEquals(ints1, new int[] {1, 3, 12, 0, 0});
    }

    private int[] moveZeroesByPoint(int[] arrays) {
        int point = 0;
        for (int i = 0; i < arrays.length; i++) {
            if (arrays[i] !=)
        }
    }

}
