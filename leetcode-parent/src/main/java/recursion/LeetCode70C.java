package recursion;

/**
 * https://leetcode.cn/problems/climbing-stairs/
 * @Author 霄池
 * @Date 2023/10/9 22:15
 */
public class LeetCode70C {

    public static void main(String[] args) {

        LeetCode70C leetCode70B = new LeetCode70C();
        int i = leetCode70B.climbStairs(3);
        System.out.println(i);

    }

    public int climbStairs(int n) {
        int i1 = 1;
        int i2 = 2;
        int i3 = 0;

        if (n == 1) {
            return i1;
        }
        if (n == 2) {
            return i2;
        }

        int i = 3;
        while (i <= n) {

            i3 = i1 + i2;

            i1 = i2;
            i2 = i3;

            i = i + 1;
        }
        return i3;
    }

}
