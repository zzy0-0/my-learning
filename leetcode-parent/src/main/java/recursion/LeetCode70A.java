package recursion;

/**
 * https://leetcode.cn/problems/climbing-stairs/
 * @Author 霄池
 * @Date 2023/10/9 22:15
 */
public class LeetCode70A {
    int i = 10;

    int count = 0;

    public static void main(String[] args) {
        int loop = loop(10);
        System.out.println(loop);

    }

    public static int loop(int currentLevel) {
        //终止条件
        if (currentLevel <= 1) {
            return 1;
        }

        // 递归
        return loop(currentLevel - 1) + loop(currentLevel - 2);

    }
}
