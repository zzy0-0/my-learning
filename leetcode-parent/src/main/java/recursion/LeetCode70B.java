package recursion;

/**
 * https://leetcode.cn/problems/climbing-stairs/
 * @Author 霄池
 * @Date 2023/10/9 22:15
 */
public class LeetCode70B {

    public static void main(String[] args) {

        LeetCode70B leetCode70B = new LeetCode70B();
        int i = leetCode70B.climbStairs(10);
        System.out.println(i);

    }

    public int climbStairs(int n) {
        int[] stairs = new int[n];
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                stairs[i] = 1;
                continue;
            }
            if (i == 1) {
                stairs[i] = 2;
                continue;
            }
            stairs[i] = stairs[i - 1] + stairs[i - 2];
        }
        return stairs[n - 1];
    }

}
