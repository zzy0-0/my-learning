package com.zzjson.aop.aspect.xml;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;


/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.aop.aspect</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年06月10日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Slf4j
public class XMLAspect {

	/*
	 * 配置前置通知,使用在方法aspect()上注册的切入点
	 * 同时接受JoinPoint切入点对象,可以没有该参数
	 */
	public void before(JoinPoint joinPoint) {
//		System.out.println(joinPoint.getArgs()); //获取实参列表
//		System.out.println(joinPoint.getKind());	//连接点类型，如method-execution
//		System.out.println(joinPoint.getSignature()); //获取被调用的切点
//		System.out.println(joinPoint.getTarget());	//获取目标对象
//		System.out.println(joinPoint.getThis());	//获取this的值

		log.info("before " + joinPoint);
	}

	//配置后置通知,使用在方法aspect()上注册的切入点
	public void after(JoinPoint joinPoint) {
		log.info("after " + joinPoint);
	}

	//配置环绕通知,使用在方法aspect()上注册的切入点
	public void around(JoinPoint joinPoint) {
		long start = System.currentTimeMillis();
		try {
			((ProceedingJoinPoint) joinPoint).proceed();
			long end = System.currentTimeMillis();
			log.info("around " + joinPoint + "\tUse time : " + (end - start) + " ms!");
		} catch (Throwable e) {
			long end = System.currentTimeMillis();
			log.info("around " + joinPoint + "\tUse time : " + (end - start) + " ms with exception : " + e.getMessage());
		}
	}

	//配置后置返回通知,使用在方法aspect()上注册的切入点
	public void afterReturn(JoinPoint joinPoint) {
		log.info("afterReturn " + joinPoint);
	}

	//配置抛出异常后通知,使用在方法aspect()上注册的切入点
	public void afterThrow(JoinPoint joinPoint, Exception ex) {
		log.info("afterThrow " + joinPoint + "\t" + ex.getMessage());
	}

}