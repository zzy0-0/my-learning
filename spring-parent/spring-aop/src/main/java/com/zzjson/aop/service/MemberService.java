package com.zzjson.aop.service;

import com.zzjson.aop.annoation.MyLog;
import com.zzjson.aop.model.Member;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.aop.service</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年06月10日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Service
@Slf4j
public class MemberService  implements IMemberService{

	@MyLog("get id()")
	public Member get(long id) {
		log.info("getMemberById method . . .");
		return new Member();
	}

	@MyLog("get ()")
	public Member get() {
		log.info("getMember method . . .");
		return new Member();
	}

	@MyLog("save member")
	public void save(Member member) {
		log.info("save member method . . .");
	}

	@MyLog("delete id ")
	public boolean delete(long id) {
		log.info("delete method . . .");
		throw new RuntimeException("spring aop ThrowAdvice演示");
	}
}