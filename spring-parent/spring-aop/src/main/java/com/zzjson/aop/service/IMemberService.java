package com.zzjson.aop.service;

import com.zzjson.aop.model.Member;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public interface IMemberService {
	Member get(long id);

	Member get();

	void save(Member member);

	boolean delete(long id);
}