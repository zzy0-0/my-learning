package com.zzjson.aop.service;


import lombok.extern.slf4j.Slf4j;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Slf4j
public class SimpleMessageServiceImpl implements MessageService {


	@Override
	public void sendMessage(String message) {
		log.info(message);
	}
}