package com.zzjson.aop.introductions;

import com.zzjson.aop.model.Member;
import com.zzjson.aop.service.IMemberService;
import com.zzjson.aop.service.MemberService;
import com.zzjson.aop.service.MessageService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Configuration
@Import({MemberService.class, AnnotationWithIntroAspect.class})
@EnableAspectJAutoProxy
public class AnnotationWithIntroAspectBootstrap {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AnnotationWithIntroAspectBootstrap.class);
		IMemberService bean = context.getBean(IMemberService.class);

		Member member = bean.get();

		//直接能够强转
		MessageService messageService = (MessageService) bean;
		messageService.sendMessage("发送消息给管理员");

	}
}