package com.zzjson.aop.autoproxy;

import com.zzjson.aop.aspect.program.proxyfactory.MethodBeforeLogAdvice;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MyAdvisor implements PointcutAdvisor {

	@Override
	public Pointcut getPointcut() {
		NameMatchMethodPointcut nameMatchMethodPointcut = new NameMatchMethodPointcut();
		nameMatchMethodPointcut.addMethodName("sendMessage");
		return nameMatchMethodPointcut;
	}

	@Override
	public Advice getAdvice() {
		return new MethodBeforeLogAdvice();
	}

	@Override
	public boolean isPerInstance() {
		return false;
	}
}