package com.zzjson.aop.autoproxy;

import com.zzjson.aop.service.MessageService;
import com.zzjson.aop.service.SimpleMessageServiceImpl;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月19日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Configuration
public class BeanNameAutoProxyBootStrap {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanNameAutoProxyBootStrap.class);
		MessageService bean = context.getBean(MessageService.class);
		bean.sendMessage("dsa");
	}

	@Bean
	public BeanNameAutoProxyCreator creator() {
		BeanNameAutoProxyCreator beanNameAutoProxyCreator = new BeanNameAutoProxyCreator();
		//指定需要代理的BeanName
		beanNameAutoProxyCreator.setBeanNames("simpleMessageServiceImpl");
		beanNameAutoProxyCreator.setInterceptorNames("methodBeforeLogAdvice", "myAdvisor");
		return beanNameAutoProxyCreator;
	}

	@Bean
	public MessageService simpleMessageServiceImpl() {
		return new SimpleMessageServiceImpl();
	}

	@Bean
	public MyAdvisor myAdvisor() {
		return new MyAdvisor();
	}

	@Bean
	public MethodBeforeLogAdvice methodBeforeLogAdvice() {
		return new MethodBeforeLogAdvice();
	}
}