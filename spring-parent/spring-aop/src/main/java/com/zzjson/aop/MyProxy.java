package com.zzjson.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月11日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MyProxy {
	public static void main(String[] args) {
		WebPaiObject webPaiObject = new WebPaiObject();

		MyMapper myMapper = (MyMapper) Proxy.newProxyInstance(WebPaiObject.class.getClassLoader(), WebPaiObject.class.getInterfaces(), new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				System.out.println("开始计数 访问次数为:" + 1);
				//////
				return method.invoke(webPaiObject, args);
				//return proxy;
			}
		});
		System.out.println(myMapper.query("我的名字"));
		//String result = myMapper.query("参数是xxx");
		//System.out.println(result);

	}

	static class WebPaiObject implements MyMapper {

		@Override
		public String query(String name) {

			//System.out.println("name" + name);
			return name + "123";
		}
	}
}