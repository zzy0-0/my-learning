package com.zzjson.aop.aspect.anno;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.aop.aspect</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年06月10日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Component
@Aspect
@Slf4j
public class ArgAspect {

	//配置切入点,该方法无方法体,主要为方便同类中其他方法使用此处配置的切入点
	@Pointcut(value = "execution(* com.zzjson.aop.service..*(..))")
	public void aspect() {
	}

	//配置前置通知,拦截返回值为com.zzjson.aop.model.Member的方法
	@Before("execution(com.zzjson.aop.model.Member com.zzjson.aop.service..*(..))")
	public void beforeReturnUser(JoinPoint joinPoint) {
		log.info("beforeReturnUser " + joinPoint);
	}

	//配置前置通知,拦截参数为com.zzjson.aop.model.Member的方法
	@Before("execution(* com.zzjson.aop.service..*(com.zzjson.aop.model.Member))")
	public void beforeArgUser(JoinPoint joinPoint) {
		log.info("beforeArgUser " + joinPoint);
	}

	//配置前置通知,拦截含有long类型参数的方法,并将参数值注入到当前方法的形参id中
	@Before("aspect()&&args(id)")
	public void beforeArgId(JoinPoint joinPoint, long id) {
		log.info("beforeArgId " + joinPoint + "\tID:" + id);
	}

}