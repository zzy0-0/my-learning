package com.zzjson.aop.aspect.xml;

import com.zzjson.aop.service.IMemberService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.aop</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年06月10日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class XMLAspectBootstrap {
	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:application-context.xml");
		IMemberService memberService = context.getBean("memberService", IMemberService.class);
		memberService.get(1);
	}
}