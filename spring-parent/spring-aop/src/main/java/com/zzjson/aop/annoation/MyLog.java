package com.zzjson.aop.annoation;

import java.lang.annotation.*;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.aop.annoation</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年06月10日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyLog {
	String value();

}