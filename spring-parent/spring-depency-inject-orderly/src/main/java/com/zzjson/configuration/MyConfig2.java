package com.zzjson.configuration;

import com.zzjson.bean.Custom;
import com.zzjson.bean.Son;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * @Author 霄池
 * @Date 2023/11/22 11:28
 */

@Configuration
public class MyConfig2 {

    @Bean
    @DependsOn("parent")
    public Custom custom(@Qualifier("parent") Son son) {

        Custom custom = new Custom();
        custom.setSon((son));

        return custom;
    }

}
