package com.zzjson.configuration;

import com.zzjson.bean.Parent;
import com.zzjson.bean.Son;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 霄池
 * @Date 2023/11/22 11:27
 */
@Configuration
public class MyConfig {
    @Bean(name = "parent")
    public Parent parent() {
        return new Son();
    }
}
