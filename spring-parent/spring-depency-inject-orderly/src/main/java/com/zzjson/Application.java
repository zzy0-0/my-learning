package com.zzjson;

import com.zzjson.bean.Custom;
import com.zzjson.configuration.MyConfig;
import com.zzjson.configuration.MyConfig2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Author 霄池
 * @Date 2023/11/22 11:25
 */
@SpringBootApplication
public class Application {

    //1. 通过 ordere 控制 configuration 的加载顺序
    //2. 通过 @DependsOn
    //3. 给Custom 增加 lazy 注解，让 parent 提前加载，他在使用到的时候再加载
    //4. 直接指定 parent 创建的 Bean 类型是 Son ，这样我们注入的时候就能够直接找到
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context
            = new AnnotationConfigApplicationContext(MyConfig2.class, MyConfig.class);
        //Son son = context.getBean(Son.class);
        //Parent parent = context.getBean(Parent.class);
        //
        //System.out.println(son == parent);
        System.out.println(context.getBean(Custom.class));
    }
}
