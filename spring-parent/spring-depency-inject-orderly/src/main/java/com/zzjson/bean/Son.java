package com.zzjson.bean;

import lombok.Data;

/**
 * @Author 霄池
 * @Date 2023/11/22 11:27
 */

@Data
public class Son extends Parent {
    private String sonName;
}
