package com.zzjson.freemarker.config;

import freemarker.template.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月07日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Component
public class FreemarkerConfiguration {

	@Autowired
	private Configuration configuration;

	@Autowired
	private UpperDirective upperDirective;

	@PostConstruct
	public void setConfiguration() {
		configuration.setSharedVariable("upper", upperDirective);
	}
}


