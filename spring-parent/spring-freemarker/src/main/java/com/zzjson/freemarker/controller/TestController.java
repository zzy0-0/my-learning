package com.zzjson.freemarker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月07日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/test")
public class TestController {
	@GetMapping("/")
	public ModelAndView modelAndView() {
		HashMap<String, Object> model = new HashMap<>();
		model.put("user", "zzjson");
		return new ModelAndView("test",model);
	}
}