package com.zzjson.pc;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月06日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Component
public class C implements InitializingBean, ApplicationContextAware {
	@Autowired
	private B b;

	public C(B b) {
		this.b = b;
		A.setB(b);
	}

	@PostConstruct
	public void test() {

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		A.setB(b);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		B bean = applicationContext.getBean(B.class);
		A.setB(b);
	}

}