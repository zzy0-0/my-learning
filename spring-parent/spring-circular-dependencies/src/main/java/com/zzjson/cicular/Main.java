package com.zzjson.cicular;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月02日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(A.class, B.class);
		System.out.println("-------");

		B bBean = annotationConfigApplicationContext.getBean(B.class);
		System.out.println("B:" + bBean);
		System.out.println(" B里面的A： " + bBean.getA());

		A bean = annotationConfigApplicationContext.getBean(A.class);
		System.out.println("A:" + bean);
		System.out.println(" A里面的B： " + bean.getB());

	}
}