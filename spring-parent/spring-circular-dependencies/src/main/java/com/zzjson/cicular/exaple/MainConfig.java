package com.zzjson.cicular.exaple;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan(basePackages = {"com.zzjson.cicular.exaple"})
@EnableAspectJAutoProxy
public class MainConfig {


}
