package com.zzjson.cicular;

import org.springframework.stereotype.Component;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月02日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Component
//@Lazy
//@DependsOn("b")
public class A {
	private B b;

	public A(B b) {
		this.b = b;
	}

	/*public A() {
	}


	@Autowired
	public void setB(B b) {
		this.b = b;
	}*/

	public B getB() {
		return b;
	}
}