package com.zzjson.datasource.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzjson.datasource.entity.UserPay;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserPayMapper extends BaseMapper<UserPay> {

	int addUserPay(UserPay userPay);

	UserPay queryByName(String name);
}
