package com.zzjson.datasource;

import com.zzjson.datasource.datasource.DynamicDataSourceConfig;
import com.zzjson.datasource.util.SpringUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

/**
 *
 */
@MapperScan("com.zzjson.datasource.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Import({DynamicDataSourceConfig.class})
public class DynamicDSApp {

	public static void main(String[] args) {
		SpringApplication.run(DynamicDSApp.class, args);
	}

	@Bean
	public SpringUtil getSpringUtil2() {
		return new SpringUtil();
	}

}
