package com.zzjson.datasource.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzjson.datasource.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

	int addUser(User user);

	User queryByName(String name);
}
