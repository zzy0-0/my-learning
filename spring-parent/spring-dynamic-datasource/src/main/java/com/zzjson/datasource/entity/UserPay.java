package com.zzjson.datasource.entity;

import lombok.Data;

@Data
public class UserPay {
	private String id;
	private String name;
	private int amount;
}
