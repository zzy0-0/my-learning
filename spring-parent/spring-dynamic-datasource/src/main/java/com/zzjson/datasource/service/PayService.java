package com.zzjson.datasource.service;


import com.zzjson.datasource.entity.User;

public interface PayService {
    public void payAndFindWithRequireNew(String userId);

    public void payAndFindWithRequireNewInnerCall(String userId);

    public void payAndFindWithRequire(String userId);

    public void payAndFindWithNested(String userId);

    public void pay2(String userId);

    public void pay3();

    public void payWithPropagationNested(User user);

    public void payWithPropagationNestedOuter(User user);

    public void payWithPropagationNew(User user);

    public void payWithPropagationNewOuter(User user1);

    public User findUser4RequireNew(String name);

    public User findUser4Require(String name);

    public User findUser4Nested(String name);

    public User findUser3(String name);
}
