package com.zzjson.datasource.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzjson.datasource.datasource.DataSource;
import com.zzjson.datasource.datasource.DataSourceNames;
import com.zzjson.datasource.entity.SysUser;
import com.zzjson.datasource.mapper.SysUserMapper;
import com.zzjson.datasource.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

	@Override
	public SysUser findUserByFirstDb(long id) {
		return this.baseMapper.selectById(id);
	}

	@DataSource(name = DataSourceNames.SLAVE)
	@Override
	public SysUser findUserBySecondDb(long id) {
		return this.baseMapper.selectById(id);
	}

}
