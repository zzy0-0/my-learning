package com.zzjson.datasource.service;

public interface OrderService {

    public void placeOrderAndFindWithRequireNew(String userId);

    public void placeOrderAndFindWithRequireNewInnerCall(String userId);

    public void placeOrderAndFindWithRequire(String userId);

    public void placeOrderAndFindNested(String userId);

    public void placeOrder2(String userId);

    public void debugDatasourceTransByThreadLocal();

    public void testPropogationNested();

    public void testPropogationNestedExceptionOuter();

    public void testPropogationNestedExceptionOuterNotRuntimeException() throws Exception;

    public void testPropogationRequireNew();

    public void testPropogationRequireNewOuter();
}
