package com.zzjson.datasource.service.impl;

import com.zzjson.datasource.datasource.DataSource;
import com.zzjson.datasource.datasource.DataSourceNames;
import com.zzjson.datasource.datasource.DynamicDataSource;
import com.zzjson.datasource.entity.User;
import com.zzjson.datasource.entity.UserPay;
import com.zzjson.datasource.mapper.UserMapper;
import com.zzjson.datasource.mapper.UserPayMapper;
import com.zzjson.datasource.service.PayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PayServiceImpl implements PayService {
    private static final Logger logger = LoggerFactory.getLogger(PayServiceImpl.class.getName());

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserPayMapper userPayMapper;

    @Autowired
    PayService payService;

    @Override
    @Transactional
    @DataSource(name = DataSourceNames.MASTER)
    public void payAndFindWithRequireNew(String userId) {
        logger.info("准备执行findUser4RequireNew方法");
        String userName = userId+ "_Name";
        User user = payService.findUser4RequireNew(userName);

        if (null == user)
        {
//            System.out.println("从从库中拿到的user为空");
            logger.info("从从库中拿到的user为空，返回");

        }else
        {
            logger.info("从从库中拿到了user：" + user.toString());
            logger.info("开始在主库中插入userPay记录!");
            //FIXME 打印SQL连接语句
            UserPay userPay1 = new UserPay();
            userPay1.setId(user.getId());
            userPay1.setName(user.getName());
            userPay1.setAmount(10);
            userPayMapper.addUserPay(userPay1);
            logger.info("在主库中插入userPay记录成功!");
        }
    }

    @Override
    @Transactional
    @DataSource(name = DataSourceNames.MASTER)
    public void payAndFindWithRequireNewInnerCall(String userId) {
        logger.info("准备执行findUser4RequireNew方法");
        String userName = userId+ "_Name";
        User user = this.findUser4RequireNew(userName);

        
        if (null == user)
        {
//            System.out.println("从从库中拿到的user为空");
            logger.info("从从库中拿到的user为空，返回");

        }else
        {
            logger.info("从从库中拿到了user：" + user.toString());
            logger.info("开始在主库中插入userPay记录!");
            //FIXME 打印SQL连接语句
            UserPay userPay1 = new UserPay();
            userPay1.setId(user.getId());
            userPay1.setName(user.getName());
            userPay1.setAmount(10);
            userPayMapper.addUserPay(userPay1);
            logger.info("在主库中插入userPay记录成功!");
        }
    }

    @Override
    @Transactional
    @DataSource(name = DataSourceNames.MASTER)
    public void payAndFindWithRequire(String userId) {
        logger.info("准备执行findUser4RequireNew方法");
        String userName = userId+ "_Name";
        User user = payService.findUser4Require(userName);

        
        if (null == user)
        {
//            System.out.println("从从库中拿到的user为空");
            logger.info("从从库中拿到的user为空，返回");

        }else
        {
            logger.info("从从库中拿到了user：" + user.toString());
            logger.info("开始在主库中插入userPay记录!");
            //FIXME 打印SQL连接语句
            UserPay userPay1 = new UserPay();
            userPay1.setId(user.getId());
            userPay1.setName(user.getName());
            userPay1.setAmount(10);
            userPayMapper.addUserPay(userPay1);
            logger.info("在主库中插入userPay记录成功!");
        }
    }

    @Override
    @Transactional
    @DataSource(name = DataSourceNames.MASTER)
    public void payAndFindWithNested(String userId) {
        logger.info("准备执行 payAndFindWithNested 方法");
        String userName = userId+ "_Name";
        User user = payService.findUser4Nested(userName);

        
        if (null == user)
        {
//            System.out.println("从从库中拿到的user为空");
            logger.info("从从库中拿到的user为空，返回");

        }else
        {
            logger.info("从从库中拿到了user：" + user.toString());
            logger.info("开始在主库中插入userPay记录!");
            //FIXME 打印SQL连接语句
            UserPay userPay1 = new UserPay();
            userPay1.setId(user.getId());
            userPay1.setName(user.getName());
            userPay1.setAmount(10);
            userPayMapper.addUserPay(userPay1);
            logger.info("在主库中插入userPay记录成功!");
        }
    }

    @Override
    @Transactional
    @DataSource(name = DataSourceNames.MASTER)
    public void pay2(String userId) {
        String userName = userId + "_Name";
        User user = payService.findUser4Require(userName);
        if (null == user)
        {
//            System.out.println("从从库中拿到的user为空");
            logger.info("从从库中拿到的user为空，返回");
        }else
        {
            logger.info("从从库中拿到了user：" + user.toString());
            logger.info("开始在主库中插入userPay记录!");
            //FIXME 打印SQL连接语句
            User user1 = new User();
            user1.setId("124");
            user1.setName("124Name");
            //插入到主库当中
            userMapper.addUser(user1);
        }
    }

    @Transactional(propagation = Propagation.NESTED)
    @DataSource(name = DataSourceNames.MASTER)
    @Override
    public void payWithPropagationNested(User user1) {
//        User user1 = new User();
//        user1.setId("128" + (System.currentTimeMillis()%100));
        System.out.println("payWithPropagationNested=" + user1.getId());
        userMapper.addUser(user1);
        throw  new RuntimeException("payWithPropagationNested throw Exception");
    }

    @Transactional(propagation = Propagation.NESTED)
    @DataSource(name = DataSourceNames.MASTER)
    @Override
    public void payWithPropagationNestedOuter(User user1) {
//        User user1 = new User();
//        user1.setId("128" + (System.currentTimeMillis()%100));
        System.out.println("payWithPropagationNested=" + user1.getId());
        userMapper.addUser(user1);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @DataSource(name = DataSourceNames.MASTER)
    @Override
    public void payWithPropagationNew(User user1){
        System.out.println("payWithPropagationNew=" + user1.getId());
        userMapper.addUser(user1);
        throw  new RuntimeException("payWithPropagationNew throw Exception");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @DataSource(name = DataSourceNames.MASTER)
    @Override
    public void payWithPropagationNewOuter(User user1) {
        System.out.println("payWithPropagationNew=" + user1.getId());
        userMapper.addUser(user1);
    }



    @Transactional
    @Override
    public void pay3() {
        User user1 = new User();
        user1.setId("124" + (System.currentTimeMillis()%100));
        user1.setName("124Name");
        userMapper.addUser(user1);

        User user2 = new User();
        user2.setId("125" + (System.currentTimeMillis()%100));
        user2.setName("125Name");
        userMapper.addUser(user2);

    }

    @Override
    @DataSource(name = DataSourceNames.SLAVE)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public User findUser4RequireNew(String name) {
        User user = userMapper.queryByName(name);
//        logger.info("findUser4RequireNew 方法执行完了，对应的dataType为{}", DynamicDataSourceContextHolder.getDateSoureType());
        logger.info("从从库中查询：findUser4RequireNew 方法执行完了，对应的dataType为{}", DynamicDataSource.CONTEXT_HOLDER.get());

        return user;
    }

    @Override
    @DataSource(name = DataSourceNames.SLAVE)
    @Transactional(propagation = Propagation.REQUIRED)
//    @Transactional
    public User findUser4Require(String name) {
        User user = userMapper.queryByName(name);
        logger.info("从从库中查询：findUser4Require 方法执行完了，对应的dataType为{}", DynamicDataSource.CONTEXT_HOLDER.get());
        return user;
    }

    @DataSource(name = DataSourceNames.SLAVE)
    @Transactional(propagation = Propagation.NESTED)
    @Override
    public User findUser4Nested(String name) {
        User user = userMapper.queryByName(name);
        logger.info("从从库中查询：findUser4Nested 方法执行完了，对应的dataType为{}", DynamicDataSource.CONTEXT_HOLDER.get());
        return user;
    }

    @Override
    //@DataSource(value = DataSourceType.SLAVE)
    public User findUser3(String name) {
        User user = userMapper.queryByName(name);
        logger.info("从从库中查询：findUser3 方法执行完了，对应的dataType为{}", DynamicDataSource.CONTEXT_HOLDER.get());
        return user;
    }
}
