package com.zzjson.datasource;


import com.zzjson.datasource.datasource.DataSource;
import com.zzjson.datasource.datasource.DataSourceNames;
import com.zzjson.datasource.entity.User;
import com.zzjson.datasource.mapper.UserMapper;
import com.zzjson.datasource.service.OrderService;
import com.zzjson.datasource.util.SpringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DynamicDSApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest
public class PropagationExceptionTest {

    private static final Logger logger = LoggerFactory.getLogger(PropagationExceptionTest.class);

    @Autowired
    UserMapper userMapper;

    @Autowired
    OrderService orderService;

    @Test
    public void testGetBean()
    {
        Object o = SpringUtil.getApplicationContext().getBean("payServiceImpl");
        System.out.println(o);
    }


    @Test
    public void testPlaceOrder2()
    {
        orderService.placeOrder2("990");
    }

    /**
     * 用来测试datasource是通过threadLocal传播的（Debug看）
     */
    @Test
    public void testDbTransByThreadLocal()
    {
        orderService.debugDatasourceTransByThreadLocal();
    }

//    @Test
//    public void testPlaceOrder3()
//    {
//        orderService.placeOrder3();
//    }
//

    /**
     * 测试nested传播属性
     */
    @Test
    public void testPropogationNested()
    {
        orderService.testPropogationNested();
    }

    @Test
    public void testPropogationNestedOuter()
    {
        orderService.testPropogationNestedExceptionOuter();
    }

    @Test
    public void testPropogationNestedOuterNotRunTimeException() throws  Exception
    {
        orderService.testPropogationNestedExceptionOuterNotRuntimeException();
    }

    /**
     * 测试nested传播属性
     */
    @Test
    public void testPropogationRequireNew()
    {
        orderService.testPropogationRequireNew();
    }

    @Test
    public void testPropogationRequireNewOuter()
    {
        orderService.testPropogationRequireNewOuter();
    }
//
//    /**
//     */
//    @Test
//    public void testInsertUserWithMapper()
//    {
//        User user = new User();
//        user.setId("128");
//        user.setName("123Name");
//        userMapper.addUser(user);
//    }

    /**
     * 无用
     */
    @Test
    @Transactional
    @DataSource(name = DataSourceNames.SLAVE)
    public void testInsertUserWithMapperToSlave()
    {
        User user = new User();
        user.setId("123");
        user.setName("123Name");
        userMapper.addUser(user);

        user.setId("124");
        user.setName("124Name");
        userMapper.addUser(user);
        System.out.println(">>>>end testInsertUserWithMapperToSlave>>>>");
    }
//
    @Test
    public void testQueryUserWithMapper()
    {
        User user = userMapper.queryByName("990_Name");
        System.out.println(user.toString());
    }

}
