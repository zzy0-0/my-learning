package com.zzjson.datasource;


import com.zzjson.datasource.datasource.DataSource;
import com.zzjson.datasource.datasource.DataSourceNames;
import com.zzjson.datasource.entity.User;
import com.zzjson.datasource.mapper.UserMapper;
import com.zzjson.datasource.service.OrderService;
import com.zzjson.datasource.util.SpringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DynamicDSApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest
public class MapperTest {

    private static final Logger logger = LoggerFactory.getLogger(MapperTest.class);

    @Autowired
    UserMapper userMapper;

    @Autowired
    OrderService orderService;


    /**
     * 测试requireNew
     */
    @Test
    public void testPlaceOrderAndFindWithRequireNew()
    {
        orderService.placeOrderAndFindWithRequireNew("990");
    }

    /**
     * 测试在payService中方法互相调用是通过this方式调用<br>
     */
    @Test
    public void testPlaceOrderAndFindWithRequireNewInnerCall()
    {
        orderService.placeOrderAndFindWithRequireNewInnerCall("990");
    }

    /**
     * 测试require
     */
    @Test
    public void testPlaceOrderAndFindWithRequire()
    {
        orderService.placeOrderAndFindWithRequire("990");
    }

    /**
     * 测试requireNested
     */
    @Test
    public void testPlaceOrderAndFindNested()
    {
        orderService.placeOrderAndFindNested("990");
    }

    /**
     * 临时测试用例<br>
     */
    @Test
    public void testGetBean()
    {
        Object o = SpringUtil.getApplicationContext().getBean("payServiceImpl");
        System.out.println(o);
    }

    /**
     * 用来测试datasource数据源是通过threadLocal传播的（Debug看）
     */
    @Test
    public void testDbTransByThreadLocal()
    {
        orderService.debugDatasourceTransByThreadLocal();
    }

    /**
     * 测试nested传播属性
     */
    @Test
    public void testPropogationNested()
    {
        orderService.testPropogationNested();
    }

    @Test
    public void testPropogationNestedOuter()
    {
        orderService.testPropogationNestedExceptionOuter();
    }

    @Test
    public void testPropogationNestedOuterNotRunTimeException() throws Exception
    {
        //here not rollback<br>
        orderService.testPropogationNestedExceptionOuterNotRuntimeException();
    }

    /**
     * 测试nested传播属性<br>
     */
    @Test
    public void testPropogationRequireNew()
    {
        orderService.testPropogationRequireNew();
    }

    @Test
    public void testPropogationRequireNewOuter()
    {
        orderService.testPropogationRequireNewOuter();
    }
//
//    /**
//     */
//    @Test
//    public void testInsertUserWithMapper()
//    {
//        User user = new User();
//        user.setId("128");
//        user.setName("123Name");
//        userMapper.addUser(user);
//    }

    @Test
    @Transactional
    @DataSource(name = DataSourceNames.SLAVE)
    public void testInsertUserWithMapperToSlave() {
        User user = new User();
        user.setId("123");
        user.setName("123Name");
        userMapper.addUser(user);

        user.setId("124");
        user.setName("124Name");
        userMapper.addUser(user);
    }
}
