package com.zzjson.web.example.controller;

import com.zzjson.web.example.config.Response;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月17日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler
	@ResponseBody
	String handleControllerException(HttpServletRequest request, Throwable ex) {
		return "出错了，异常为" + ex.getMessage();
	}

	@ResponseBody
	@ExceptionHandler({Exception.class, IOException.class})
	String handleControllerException(Throwable throwable) {
		return "出错了，异常为" + throwable.getMessage();
	}

	@ResponseBody
	@ExceptionHandler(RuntimeException.class)
	@Order(Integer.MIN_VALUE)
	String handleControllerException1(Throwable throwable) {
		return "出错了，异常为12312321" + throwable.getMessage();
	}



	/*@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new MyStringEditor());
	}
*/
	/*static class MyStringEditor extends StringEditor {
		@Override
		public void setValue(Object value) {
			value += "1231";
			super.setValue(value);
		}

	}*/

	@GetMapping("/test")
	public String test(Integer name) {
		System.out.println(name);
		throw new RuntimeException("213");
		//return "123";
	}

	@GetMapping("/date/test")
	public Response dateTest() {
		Response response = new Response();
		response.setName(new Date());
		return response;
	}

}