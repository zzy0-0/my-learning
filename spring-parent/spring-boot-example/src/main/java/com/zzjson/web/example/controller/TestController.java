package com.zzjson.web.example.controller;

import com.zzjson.web.example.WebExampleBootstrap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月17日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
public class TestController extends  Fa{
	@GetMapping("/test1")
	public String test() {
		throw new RuntimeException("213");
		//return "123";
	}

	public static void main(String[] args) {
		System.out.println(Fa.class.isAssignableFrom(TestController.class));
		System.out.println(WebExampleBootstrap.class.isAssignableFrom(TestController.class));
	}
}

class Fa{

}