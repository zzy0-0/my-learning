package com.zzjson;

import lombok.Data;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * @Author 霄池
 * @Date 2023/10/19 11:01
 * <p>
 * <p>
 * [5.0 版本才支持 supplier]用于验证 supplier 创建Bean ，scope 指定为 prototype,创建的都是同一个对象 </>
 * <p>
 * </p>
 * @see org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory#createBeanInstance}
 */

public class BeanProtoTypeCreateTest {

    @Test
    public void testScope() {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition();
        rootBeanDefinition.setBeanClass(User.class);
        rootBeanDefinition.setScope(BeanDefinition.SCOPE_PROTOTYPE);

        beanFactory.registerBeanDefinition("user", rootBeanDefinition);

        Assert.assertNotSame(beanFactory.getBean(User.class), beanFactory.getBean(User.class));

    }

    @Test
    public void testScopeAndSupplierOneInstance() {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition();
        rootBeanDefinition.setBeanClass(User.class);
        rootBeanDefinition.setScope(BeanDefinition.SCOPE_PROTOTYPE);

        User user = new User();
        rootBeanDefinition.setInstanceSupplier(() -> user);

        beanFactory.registerBeanDefinition("user", rootBeanDefinition);

        Assert.assertSame(beanFactory.getBean(User.class), beanFactory.getBean(User.class));

    }

    @Test
    public void testScopeAndSupplierCreate() {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition();
        rootBeanDefinition.setBeanClass(User.class);
        rootBeanDefinition.setScope(BeanDefinition.SCOPE_PROTOTYPE);

        //下面这两种写法等价
        //rootBeanDefinition.setInstanceSupplier(() -> new User());
        rootBeanDefinition.setInstanceSupplier(User::new);

        beanFactory.registerBeanDefinition("user", rootBeanDefinition);

        Assert.assertNotSame(beanFactory.getBean(User.class), beanFactory.getBean(User.class));

    }

    @Data
    static class User {
        private String name;
    }

}
