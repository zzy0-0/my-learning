package com.zzjson.utils;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年05月11日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class RabbitUtils {
	private static ConnectionFactory connectionFactory = new ConnectionFactory();

	static {
		connectionFactory.setHost("my-server");
		connectionFactory.setPort(5672);//5672是RabbitMQ的默认端口号
		connectionFactory.setUsername("bq123");
		connectionFactory.setPassword("bq123");
		connectionFactory.setVirtualHost("/baiqi");
	}

	public static Connection getConnection() {
		Connection conn = null;
		try {
			conn = connectionFactory.newConnection();
			return conn;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}