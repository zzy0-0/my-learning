package com.zzjson.common.vo;

public interface ErrorType {
	/**
	 * 返回code
	 *
	 * @return
	 */
	String getStatus();

	/**
	 * 返回mesg
	 *
	 * @return
	 */
	String getMsg();
}
