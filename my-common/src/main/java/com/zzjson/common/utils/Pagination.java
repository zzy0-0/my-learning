package com.zzjson.common.utils;

import lombok.Data;

import java.io.Serializable;

@Data
public class Pagination implements Serializable {
    private static final long serialVersionUID = 8177854744199481598L;

    /**
     * 分页索引,当前的页
     **/
    private int pageIndex = 1;
    /**
     * 每页记录条数
     */
    private int pageSize = 20;

    public void setPageIndex(int pageIndex) {
        if (pageIndex > 0) {
            this.pageIndex = pageIndex;
        }
    }

    public void setPageSize(int pageSize) {
        if (pageSize > 0) {
            this.pageSize = pageSize;
        }
    }

    public int getLimit() {
        return this.getPageSize();
    }

    public long getStart() {
        return Math.max(0, (this.getPageIndex() - 1) * this.getPageSize());
    }
}