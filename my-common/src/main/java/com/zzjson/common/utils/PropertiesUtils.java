package com.zzjson.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 配置文件加载 </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月02日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Slf4j
public class PropertiesUtils {

	public static Properties loadProperties(String baseName) {
		ResourceBundle application = ResourceBundle.getBundle(baseName);
		Enumeration<String> keys = application.getKeys();
		Properties properties = new Properties();
		while (keys.hasMoreElements()) {
			String realKey = keys.nextElement();
			properties.put(realKey, application.getObject(realKey));
		}
		log.info("load properties {}", application);
		return properties;
	}
}