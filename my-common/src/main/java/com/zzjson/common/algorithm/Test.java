package com.zzjson.common.algorithm;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年05月06日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class Test {

	public static void func(int[] a, int b) {
		if (a == null) {
			throw new IllegalArgumentException("数组不能为空");
		}
		int k = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] != b) {
				if (i != k) {
					swapValue(a, k, i);
				}
				k++;
			}
		}
	}

	private static void swapValue(int[] a, int k, int i) {
		int tmp;
		tmp = a[i];
		a[i] = a[k];
		a[k] = tmp;
	}

	public static void main(String[] args) {
		int[] a = {1, 2, 2, 3, 2, 4, 1};
		func(a, 2);
		printResult(a);
	}

	private static void printResult(int[] a) {
		for (int i : a) {
			System.out.println(i);
		}
	}
}