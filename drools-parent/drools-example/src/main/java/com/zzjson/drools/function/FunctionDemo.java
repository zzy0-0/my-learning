package com.zzjson.drools.function;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.function</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class FunctionDemo extends BaseTest {

	@Test
	public void testFunction() {
		KieSession kieSession = this.getKieSessionBySessionName("function-rules");

		kieSession.fireAllRules();
		kieSession.dispose();
	}

}