package com.zzjson.drools.lhs;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.lhs</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class LhsTest extends BaseTest {
	@Test
	public void testLhs() {
		KieSession kieSession = getKieSessionBySessionName("lhs-rules");

		Person person = new Person();
		person.setAge(10);

		Person person1 = new Person();
		person1.setAge(15);

		kieSession.insert(person1);
		kieSession.insert(person);


		kieSession.fireAllRules();
		kieSession.dispose();
	}

}