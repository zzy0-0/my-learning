package com.zzjson.drools;

import org.kie.api.KieServices;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月09日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class BaseTest {


	protected KieSession getKieSession() {
		KieServices kieServices = KieServices.get();
		KieContainer kieContainer = kieServices.getKieClasspathContainer();
		return kieContainer.newKieSession("all-rules");
	}

	protected KieSession getKieSession(String agendaGroupName) {
		KieSession kieSession = getKieSession();
		kieSession.getAgenda().getAgendaGroup(agendaGroupName).setFocus();
		return kieSession;
	}

	protected StatelessKieSession getStatelessKieSession() {
		KieContainer kieContainer = getKieContainer();
		StatelessKieSession kieSession = kieContainer.newStatelessKieSession("stateless-rules");

		return kieSession;
	}

	protected KieContainer getKieContainer() {
		KieServices kieServices = KieServices.get();
		KieContainer kieContainer = kieServices.getKieClasspathContainer();
		Results verify = kieContainer.verify();
		System.out.println(verify.getMessages());
		return kieContainer;
	}


	protected KieSession getKieSessionBySessionName(String sessionName) {
		KieContainer kieContainer = getKieContainer();
		KieSession kieSession = kieContainer.newKieSession(sessionName);
		return kieSession;
	}
}