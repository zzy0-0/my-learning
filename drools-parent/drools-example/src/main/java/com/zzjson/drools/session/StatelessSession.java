package com.zzjson.drools.session;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.StatelessKieSession;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.session</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class StatelessSession extends BaseTest {
	@Test
	public void testStateLessSession() {
		StatelessKieSession statelessKieSession = this.getStatelessKieSession();
		List<Person> list = new ArrayList<>();

		Person p = new Person();
		p.setAge(35);
		list.add(p);
		Person p1 = new Person();
		p1.setAge(20);
		list.add(p1);

		statelessKieSession.execute(p);

		statelessKieSession.execute(list);
	}
}