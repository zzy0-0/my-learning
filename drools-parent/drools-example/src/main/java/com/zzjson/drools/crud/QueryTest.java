package com.zzjson.drools.crud;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.crud</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class QueryTest extends BaseTest {
	@Test
	public void query() {
		KieSession kieSession = this.getKieSessionBySessionName("query-rules");
		Person p1 = new Person();
		p1.setAge(20);
		p1.setName("tom");

		Person p2 = new Person();
		p2.setAge(23);
		p2.setName("mic");

		kieSession.insert(p1);
		kieSession.insert(p2);
		int i1 = kieSession.fireAllRules();
		System.out.println("fireCount=" + i1);
		QueryResults queryResults = kieSession.getQueryResults("query-by-age");
		System.out.println("querySize is " + queryResults.size());
		for (QueryResultsRow queryResult : queryResults) {
			Person p = (Person) queryResult.get("$p1");
			System.out.println(p);
		}

		System.out.println(" ------------------------------------------------------");

		QueryResults queryResults1 = kieSession.getQueryResults("query-by-param", 21);
		System.out.println("queryResults1 size is " + queryResults1.size());
		for (QueryResultsRow row : queryResults1) {
			Person person = (Person) row.get("$p2");
			System.out.println("Person name is " + person.getName());
		}

		int i = kieSession.fireAllRules();

	}

}