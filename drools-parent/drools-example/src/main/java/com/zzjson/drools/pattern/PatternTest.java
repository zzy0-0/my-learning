package com.zzjson.drools.pattern;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Car;
import com.zzjson.drools.model.SubPerson;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.pattern</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class PatternTest extends BaseTest {
	@Test
	public void testPattern() {
		KieSession kieSession = getKieSessionBySessionName("pattern-rules");

		Car car = new Car();
		SubPerson subPerson = new SubPerson();
		subPerson.setAge(10);
		car.setSubPerson(subPerson);

//        Person person = new Person();
		kieSession.insert(car);

		kieSession.fireAllRules();
		kieSession.dispose();
	}

}