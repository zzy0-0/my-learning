package com.zzjson.drools.mapandlist;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.mapandlist</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MapAndListTest extends BaseTest {
	@Test
	public void testMapAndList() {
		KieSession kieSession = this.getKieSessionBySessionName("mapAndList-rules");
		Map<String, Integer> map = new HashMap<>();
		map.put("a", 1);
		map.put("b", 2);

		List<Person> list = new ArrayList<>();
		Person p = new Person();
		p.setAge(18);
		list.add(p);
		Person p1 = new Person();
		p1.setAge(20);
		list.add(p1);

		kieSession.insert(map);
		kieSession.insert(list);

		System.out.println(kieSession.fireAllRules());
		kieSession.dispose();
	}

}