package com.zzjson.drools.activation_group;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.activation_group</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class ActivationGroupTest extends BaseTest {
	@Test
	public void testActivationGroup() {

		KieSession kieSession = this.getKieSessionBySessionName("activationGroup-rules");
		kieSession.fireAllRules();
		kieSession.dispose();
	}
}