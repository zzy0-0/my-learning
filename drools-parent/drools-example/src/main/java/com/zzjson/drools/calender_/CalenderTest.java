package com.zzjson.drools.calender_;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.time.Calendar;
import org.quartz.impl.calendar.WeeklyCalendar;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.calender_</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class CalenderTest extends BaseTest {

	private static final Calendar WEEKDAY = new Calendar() {
		@Override
		public boolean isTimeIncluded(long timestamp) {
			WeeklyCalendar weeklyCalendar = new WeeklyCalendar();
			weeklyCalendar.setDaysExcluded(new boolean[]{false, false, false, false, false, false, false});
			weeklyCalendar.setDayExcluded(java.util.Calendar.TUESDAY, true);
			return weeklyCalendar.isTimeIncluded(timestamp);
		}
	};

	private static final Calendar WEEKDAY_EXCLUDE = new Calendar() {
		@Override
		public boolean isTimeIncluded(long timestamp) {
			WeeklyCalendar weeklyCalendar = new WeeklyCalendar();
			weeklyCalendar.setDaysExcluded(new boolean[]{false, false, false, false, false, false, false});
			return weeklyCalendar.isTimeIncluded(timestamp);
		}
	};

	@Test
	public void testCalender() {
		KieSession kieSession = getKieSessionBySessionName("calenderTest-rules");
		kieSession.getCalendars().set("weekday", WEEKDAY);
		kieSession.getCalendars().set("weekday_exclude", WEEKDAY_EXCLUDE);
		kieSession.fireAllRules();
		kieSession.dispose();
	}
}