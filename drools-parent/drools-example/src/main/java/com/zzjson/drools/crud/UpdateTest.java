package com.zzjson.drools.crud;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.crud</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class UpdateTest extends BaseTest {
	@Test
	public void testInsert() {
		KieSession kieSession = this.getKieSessionBySessionName("updateDemo-rules");
		Person p = new Person();
		p.setAge(24);
		FactHandle factHandle = kieSession.insert(p);

		kieSession.fireAllRules();
		p.setAge(25);

		kieSession.update(factHandle, p);

		kieSession.dispose();
	}

}