package com.zzjson.drools.conditional;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Car;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.conditional</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class ConditionalTest extends BaseTest {
	@Test
	public void test() {
		KieSession kieSession = getKieSessionBySessionName("conditional-demo");
		Person person = new Person();
		person.setAge(19);

		Car car = new Car();
		car.setDiscount(80);

		kieSession.insert(person);
		kieSession.insert(car);

		int i = kieSession.fireAllRules();
		System.out.println("count = " + i);

		kieSession.dispose();
	}

}