package com.zzjson.drools.fact;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.fact</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class FactHandlerTest extends BaseTest {
	@Test
	public void testFactHandler() {
		KieSession kieSession = this.getKieSession("fact-handler-group");
		Person person = new Person();
		person.setName("dd");
		person.setAge(90);
		//对同一个对象的修改需要使用 factHandler
		FactHandle handle = kieSession.insert(person);

		System.out.println(handle.toExternalForm());

		int count = kieSession.fireAllRules();
		System.out.println("fire count=" + count);

		person.setAge(81);

		//一旦触发之后，再次触发需要重新设置为焦点
		kieSession.getAgenda().getAgendaGroup("fact-handler-group").setFocus();
		kieSession.update(handle, person);

		count = kieSession.fireAllRules();
		System.out.println("fire count=" + count);
		kieSession.dispose();

		Person person1 = (Person) kieSession.getObject(handle);
		System.out.println(person1);


	}
}