package com.zzjson.drools.loop;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Assert;
import org.junit.Test;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.runtime.KieSession;

import java.util.Collection;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.loop</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class NoLoopTest extends BaseTest {
	@Test
	public void testNoLoop() {
		KieSession kieSession = this.getKieSessionBySessionName("no-loop-rules");
		Collection<KiePackage> kiePackages = kieSession.getKieBase().getKiePackages();
		for (KiePackage kiePackage : kiePackages) {
			String name = kiePackage.getName();
			Collection<Rule> rules = kiePackage.getRules();
			System.out.println(rules);
			System.out.println(name);
		}
		Person p = new Person();
		p.setAge(5);
		kieSession.insert(p);
		int count = kieSession.fireAllRules();
		// Assert.assertEquals(10 - 5, count);
		Assert.assertEquals(1, count);
		kieSession.dispose();
	}

}