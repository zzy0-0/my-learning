package com.zzjson.drools;

import com.zzjson.drools.model.Car;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月09日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class DroolsApiTest extends BaseTest {
	@Test
	public void test() {
		KieSession kieSession = getKieSession("test-drools7");
		//language=JSON
		String s = "";
		Person p1 = new Person();
		p1.setAge(30);

		Car c1 = new Car();
		c1.setPerson(p1);

		Person p2 = new Person();
		p2.setAge(90);

		Car c2 = new Car();
		c2.setPerson(p2);

		kieSession.insert(c1);
		kieSession.insert(c2);
		int count = kieSession.fireAllRules();
		kieSession.dispose();

		System.out.println("Fire " + count + " rule(s)!");

		System.out.println("The discount of c1 is " + c1.getDiscount() + "%");
		System.out.println("The discount of c2 is " + c2.getDiscount() + "%");
	}
}