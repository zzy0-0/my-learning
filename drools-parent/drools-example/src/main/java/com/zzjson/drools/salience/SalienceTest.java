package com.zzjson.drools.salience;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.salience</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class SalienceTest extends BaseTest {
	@Test
	public void testSalience() {
		KieSession kieSession = this.getKieSessionBySessionName("salience-rules");
		Person person = new Person();
		person.setAge(10);
		kieSession.insert(person);

		kieSession.fireAllRules();
		kieSession.dispose();
	}

}