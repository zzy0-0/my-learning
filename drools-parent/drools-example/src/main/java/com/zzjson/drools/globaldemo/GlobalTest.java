package com.zzjson.drools.globaldemo;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.global</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月15日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class GlobalTest extends BaseTest {

	@Test
	public void testGlobal() {
		KieSession kieSession = this.getKieSessionBySessionName("global-rules");

		EmailService emailService = new EmailService();
		kieSession.setGlobal("emailservice", emailService);

		kieSession.fireAllRules();
		kieSession.dispose();
	}
}