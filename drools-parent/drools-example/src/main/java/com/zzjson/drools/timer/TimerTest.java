package com.zzjson.drools.timer;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Server;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.timer</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class TimerTest extends BaseTest {
	@Test
	public void testTimer() throws InterruptedException {

		KieSession kieSession = this.getKieSessionBySessionName("timerTest-rules");

		Server server = new Server();
		server.setTimes(0);

		new Thread(() -> kieSession.fireUntilHalt()).start();

		FactHandle factHandle = kieSession.insert(server);
		for (int i = 1; i < 10; i++) {
			System.out.println(i);
			Thread.sleep(1000);
			server.setTimes(i);
			//kieSession.update(factHandle, server);
		}

		Thread.sleep(3000);
		kieSession.halt();
		System.out.println("server尝试结果：" + server.getResult());

		//language=JSON
		String s = "package com.timerTest\nimport com.zzjson.drools.model.Server\nimport java.util.Date\n\nrule \"test-timer\"\ntimer (cron:* 0/1 * * * ? )\nwhen\n$s:Server(times > 5)\nthen\nSystem.out.println(\"已经尝试\" + $s.getTimes() + \"次，超过预警次数！\");\n$s.setResult(new Date() + \"--服务器已经尝试\" + $s.getTimes() + \"次,依旧失败，发出警报！\");\nend";
	}
}