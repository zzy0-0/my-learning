package com.zzjson.drools.loop;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.loop</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class LockOnActiveTest extends BaseTest {
	@Test
	public void testLockOnActive() {
		KieSession kieSession = this.getKieSessionBySessionName("lock-on-active-rules");

		Person p = new Person();
		p.setAge(19);

		kieSession.insert(p);
		kieSession.fireAllRules();
		kieSession.dispose();
	}
}