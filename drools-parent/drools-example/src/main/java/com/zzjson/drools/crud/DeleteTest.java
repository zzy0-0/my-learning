package com.zzjson.drools.crud;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.crud</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class DeleteTest extends BaseTest {
	@Test
	public void testDelete() {
		KieSession kieSession = this.getKieSessionBySessionName("deleteDemo-rules");

		Person p = new Person();
		p.setAge(21);
		FactHandle factHandle = kieSession.insert(p);
		// kieSession.delete(factHandle);

		int i = kieSession.fireAllRules();
		System.out.println(i);

		kieSession.dispose();
	}
}