package com.zzjson.drools.meta;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.meta</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月16日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MetaTest extends BaseTest {
	@Test
	public void metaTest() {
		KieSession kieSession = this.getKieSessionBySessionName("meta-rules");

		kieSession.fireAllRules();
		kieSession.dispose();
	}

}