package com.zzjson.drools.rowflowgroup;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.definition.KiePackage;
import org.kie.api.runtime.KieSession;

import java.util.Collection;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.rowflowgroup</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class RowFlowGroupTest extends BaseTest {
	@Test
	public void testRowFlowGroup() {
		KieSession kieSession = this.getKieSessionBySessionName("ruleFlowGroup-rules");
		KieBase kieBase = kieSession.getKieBase();
		Collection<KiePackage> kiePackages = kieBase.getKiePackages();
		for (KiePackage kiePackage : kiePackages) {
			System.out.println(kiePackage.getName());

		}
		kieSession.getAgenda().getAgendaGroup("rule-flow-group-1").setFocus();

		System.out.println(kieSession.fireAllRules());
		kieSession.getAgenda().getAgendaGroup("rule-flow-group-2").setFocus();
		System.out.println(kieSession.fireAllRules());


		kieSession.getAgenda().getAgendaGroup("rule-flow-group-1").setFocus();
		System.out.println(kieSession.fireAllRules());

		kieSession.getAgenda().getAgendaGroup("rule-flow-group-2").setFocus();
		System.out.println(kieSession.fireAllRules());
		kieSession.dispose();

	}
}