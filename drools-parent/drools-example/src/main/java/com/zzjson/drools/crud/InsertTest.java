package com.zzjson.drools.crud;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.crud</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class InsertTest extends BaseTest {
	@Test
	public void testInsert() {
		KieSession kieSession = this.getKieSessionBySessionName("insertDemo-rules");

		kieSession.fireAllRules();
		kieSession.dispose();
	}

}