package com.zzjson.drools.crud;

import com.zzjson.drools.BaseTest;
import com.zzjson.drools.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.crud</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class ModifyTest extends BaseTest {

	@Test
	public void testModify() {
		KieSession kieSession = this.getKieSessionBySessionName("modifyDemo-rules");

		Person p = new Person();
		p.setAge(21);

		kieSession.insert(p);

		kieSession.fireAllRules();
		kieSession.dispose();
	}

}