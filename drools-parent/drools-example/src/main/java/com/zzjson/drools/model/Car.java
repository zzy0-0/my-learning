package com.zzjson.drools.model;

import lombok.Data;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.model</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月09日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Data
public class Car {
	private int discount = 100;

	private SubPerson subPerson;

	private Person person;
}