package com.zzjson.drools.agendagroup;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.agendagroup</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class AgendaGroupTest extends BaseTest {
	@Test
	public void testAgendaGroup() {

		KieSession kieSession = this.getKieSessionBySessionName("agendaGroup-rules");
		kieSession.getAgenda().getAgendaGroup("agenda-group-test").setFocus();
		kieSession.fireAllRules();
		kieSession.dispose();
	}
}