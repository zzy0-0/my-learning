package com.zzjson.drools.dateeffective;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.dateeffective</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class DateEffectiveTest extends BaseTest {
	@Test
	public void testDateEffective() {
		System.setProperty("drools.dateformat", "yyyy-MM-dd HH:mm");
		KieSession kieSession = this.getKieSessionBySessionName("dateEffective-rules");
		kieSession.fireAllRules();
		kieSession.dispose();
	}

}