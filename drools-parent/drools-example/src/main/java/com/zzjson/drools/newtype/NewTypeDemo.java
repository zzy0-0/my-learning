package com.zzjson.drools.newtype;

import com.zzjson.drools.BaseTest;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.definition.type.FactType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * <p>****************************************************************************</p>
 * 
 * <ul style="margin:15px;">
 * <li>Description : com.zzjson.drools.newtype</li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2019年08月15日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class NewTypeDemo extends BaseTest {
	@Test
	public void testNewType() throws IllegalAccessException, InstantiationException {
		KieContainer kieContainer = this.getKieContainer();
		KieSession kieSession = kieContainer.newKieSession("new-type-rules");
		KieBase kieBase = kieContainer.getKieBase("new-type-kbase");
		FactType factType = kieBase.getFactType("com.newType", "Address");
		Object country = factType.newInstance();
		factType.set(country,"number",20);
		factType.set(country,"name","北京");
		kieSession.insert(country);


		kieSession.fireAllRules();
		kieSession.dispose();
	}

}