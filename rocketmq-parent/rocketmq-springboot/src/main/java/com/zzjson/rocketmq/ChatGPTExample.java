package com.zzjson.rocketmq;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class ChatGPTExample {

    private static final String API_ENDPOINT = "https://api.openai.com/v1/engines/davinci-codex/completions";
    private static final String ACCESS_TOKEN = "<your-access-token>";

    public static void main(String[] args) throws IOException {
        // Create a new HTTP client
        CloseableHttpClient httpClient = HttpClients.createDefault();

        // Set the API request parameters
        String prompt = "Hello, how are you today?";
        int maxTokens = 60;
        double temperature = 0.7;
        double topP = 1.0;
        double frequencyPenalty = 0.5;
        double presencePenalty = 0.0;

        // Create a new HTTP POST request
        HttpPost httpPost = new HttpPost(API_ENDPOINT);
        httpPost.addHeader("Content-Type", "application/json");
        httpPost.addHeader("Authorization", "Bearer " + ACCESS_TOKEN);

        // Set the request body as a JSON string
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper.writeValueAsString(
            new ChatGPTRequest(prompt, maxTokens, temperature, topP, frequencyPenalty, presencePenalty));
        httpPost.setEntity(new StringEntity(requestBody));

        // Send the API request and parse the response
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();
        String responseBody = EntityUtils.toString(entity);
        EntityUtils.consume(entity);
        response.close();

        JsonNode responseJson = objectMapper.readTree(responseBody);
        String responseText = responseJson.get("choices").get(0).get("text").asText();

        // Print the response text to the console
        System.out.println("ChatGPT response: " + responseText);

        // Close the HTTP client
        httpClient.close();
    }

    static class ChatGPTRequest {
        public String prompt;
        public int max_tokens;
        public double temperature;
        public double top_p;
        public double frequency_penalty;
        public double presence_penalty;

        public ChatGPTRequest(String prompt, int maxTokens, double temperature, double topP,
                              double frequencyPenalty, double presencePenalty) {
            this.prompt = prompt;
            this.max_tokens = maxTokens;
            this.temperature = temperature;
            this.top_p = topP;
            this.frequency_penalty = frequencyPenalty;
            this.presence_penalty = presencePenalty;
        }
    }
}