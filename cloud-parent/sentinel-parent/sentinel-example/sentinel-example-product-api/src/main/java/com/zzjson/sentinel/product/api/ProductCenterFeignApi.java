package com.zzjson.sentinel.product.api;

import com.zzjson.common.entity.ProductInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 通过java配置版本来指定细粒度配置
 */

@FeignClient(name = "product-center")
public interface ProductCenterFeignApi {

	@RequestMapping("/selectProductInfoById/{productNo}")
	ProductInfo selectProductInfoById(@PathVariable("productNo") String productNo);
}
