package com.zzjson.sentinel.order.controller;

import org.springframework.web.client.RestTemplate;

import java.util.Date;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 测试SentinelResource的注解(可以优化) </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class TestSentinelRule {

	public static void main(String[] args) throws InterruptedException {
/*        RestTemplate restTemplate = new RestTemplate();
        for(int i=0;i<1000;i++) {
            restTemplate.postForObject("http://my-server:8080/saveOrder",null,String.class);
            //Thread.sleep(200);
        }*/

		testWaiting();
	}

	public static void testWaiting() {
		RestTemplate restTemplate = new RestTemplate();
		for (int i = 0; i < 1000; i++) {
			restTemplate.postForObject("http://my-server:8080/findAll", null, String.class);
			System.out.println(new Date().getTime());
		}
	}
}
