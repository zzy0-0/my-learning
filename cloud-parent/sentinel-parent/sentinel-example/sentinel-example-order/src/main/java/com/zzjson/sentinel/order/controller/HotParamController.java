package com.zzjson.sentinel.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 热点参数 </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@Slf4j
public class HotParamController {

	@SentinelResource(value = "order-skill")
	@RequestMapping("/orderSave")
	public String saveOrder(@RequestParam(value = "userId", required = false) String userId,
							@RequestParam(value = "productId", required = false) String productId) {
		log.info("userId:{}", userId);
		log.info("productId:{}", productId);
		return userId;
	}

}
