package com.zzjson.sentinel.order.controller.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Slf4j
public class GlobalBlockHandler {

	/**
	 * 该方法用来处理限流的方法
	 */
	public static String blockHandlerMethod(String testParam, BlockException ex) {
		log.info("被限流量了..." + ex.getMessage());
		return "invoke block......";
	}

}
