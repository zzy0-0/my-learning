package com.zzjson.sentinel.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.zzjson.sentinel.order.controller.handler.GlobalFallbackHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 测试SentinelResource的注解 </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@Slf4j
public class SentinelResource_Great_Controller {


    @SentinelResource(value = "testSentinelResourceGreat",
            //blockHandlerClass = GlobalBlockHandler.class,
            fallbackClass = GlobalFallbackHandler.class,
            //blockHandler = "blockHandlerMethod"
            fallback = "fallBackHandlerMethod"
    )
    @RequestMapping("/testSentinelApi-great")
    public String testSentinelResource(@RequestParam(value = "testParam",required = false) String testParam) throws InterruptedException {

/*        if(testParam == null) {
            throw new IllegalArgumentException("param can not be null");
        }*/
        Thread.sleep(100);
        return "you param is:"+testParam;
    }
}
