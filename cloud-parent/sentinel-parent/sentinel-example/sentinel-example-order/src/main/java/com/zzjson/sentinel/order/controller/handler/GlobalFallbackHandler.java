package com.zzjson.sentinel.order.controller.handler;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Slf4j
public class GlobalFallbackHandler {

	/**
	 * 用来处理容错降级的方法
	 */
	public static String fallBackHandlerMethod(String testParam, Throwable ex) {
		log.info("被降级拉......" + ex.getMessage());
		return "invoke fallback......";
	}
}
