package com.zzjson.sentinel.order.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 降级策略controller </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@Slf4j
public class DeGradeDemoController {

	@RequestMapping("/testRt")
	public String testRTDeGrade() {
		log.info("rt 降级策略");
		return "rt";
	}

	@RequestMapping("/testExProp")
	public String testExceptionProportion() {
		log.info("异常比例");
		//numA[0-3]
		Integer numA = new Random().nextInt(4);
		//请求十次概率>1的概率是75%
		if (numA > 1) {
			log.info("会抛出异常");
			throw new RuntimeException("throw  exception");
		}
		return "testExProp";
	}

	@RequestMapping("/testExCount")
	public String testExCount() {
		log.info("异常比例");
		//numA[0-3]
		Integer numA = new Random().nextInt(4);
		//请求十次概率>1的概率是75%
		if (numA > 1) {
			log.info("会抛出异常");
			throw new RuntimeException("throw  exception");
		}
		return "testExCount";
	}
}
