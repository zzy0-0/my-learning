package com.zzjson.sentinel.product.controller;

import com.zzjson.common.entity.ProductInfo;
import com.zzjson.dao.mapper.ProductInfoMapper;
import com.zzjson.sentinel.product.api.ProductCenterFeignApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月08日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
public class ProductInfoController implements ProductCenterFeignApi {


	@Autowired
	private ProductInfoMapper productInfoMapper;

	@Override
	@RequestMapping("/selectProductInfoById/{productNo}")
	public ProductInfo selectProductInfoById(@PathVariable("productNo") String productNo) {

		ProductInfo productInfo = productInfoMapper.selectById(productNo);
		return productInfo;
	}
}