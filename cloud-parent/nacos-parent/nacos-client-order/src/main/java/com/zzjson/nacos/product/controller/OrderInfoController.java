package com.zzjson.nacos.product.controller;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.cloud.nacos.discovery.NacosServiceDiscovery;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.zzjson.common.entity.OrderInfo;
import com.zzjson.common.entity.ProductInfo;
import com.zzjson.common.vo.OrderVo;
import com.zzjson.dao.mapper.OrderInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月28日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
public class OrderInfoController {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private OrderInfoMapper orderInfoMapper;

	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private NacosServiceManager nacosServiceManager;
	@Autowired
	private NacosDiscoveryProperties discoveryProperties;

	@RequestMapping("/selectOrderInfoById/{orderNo}")
	public Object selectOrderInfoById(@PathVariable("orderNo") String orderNo) {

		OrderInfo orderInfo = orderInfoMapper.selectById(orderNo);
		if (null == orderInfo) {
			return "根据orderNo:" + orderNo + "查询没有该订单";
		}

		/**
		 * 从nacos server获取 product-info的地址
		 */
		String serviceId = "nacos-client-product";
		//List<ServiceInstance> serviceInstanceList = discoveryClient.getInstances(serviceId);

		//解决nacos cluster的问题
		List<ServiceInstance> serviceInstanceList = getServiceInstanceWithCluster(serviceId);


		if (null == serviceInstanceList || serviceInstanceList.isEmpty()) {
			return "用户微服务没有对应的实例可用";
		}
		/**
		 * 获取第0个元素
		 */
		String targetUri = serviceInstanceList.get(0).getUri().toString();

		ResponseEntity<ProductInfo> responseEntity = restTemplate.getForEntity(targetUri + "/selectProductInfoById/" + orderInfo.getProductNo(), ProductInfo.class);

		ProductInfo productInfo = responseEntity.getBody();

		if (productInfo == null) {
			return "没有对应的商品";
		}

		OrderVo orderVo = new OrderVo();
		orderVo.setOrderNo(orderInfo.getOrderNo());
		orderVo.setUserName(orderInfo.getUserName());
		orderVo.setProductName(productInfo.getProductName());
		orderVo.setProductNum(orderInfo.getProductCount());

		return orderVo;
	}

	private List<ServiceInstance> getServiceInstanceWithCluster(String serviceId) {
		NamingService namingService = nacosServiceManager
				.getNamingService(discoveryProperties.getNacosProperties());
		try {
			List<Instance> allInstances = namingService.getAllInstances(serviceId, discoveryProperties.getGroup(), Collections.singletonList(discoveryProperties.getClusterName()), true);
			return NacosServiceDiscovery.hostToServiceInstanceList(allInstances, serviceId);
		} catch (NacosException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	@GetMapping("/getServiceList")
	public List<ServiceInstance> getServiceList() {
		List<ServiceInstance> serviceInstanceList = discoveryClient.getInstances("order-center");
		return serviceInstanceList;
	}
}