package com.zzjson.nacos.order.controller;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 手动注册到nacos </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/nacos")
public class NacosController {

	@NacosInjected
	private NamingService namingService;

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseBody
	public List<Instance> get(@RequestParam String serviceName) throws NacosException {

		return namingService.getAllInstances(serviceName);
	}

	@RequestMapping("/register")
	public String register() throws NacosException {
		namingService.registerInstance("order-center-springboot", "my-server", 8084);
		return "ok";
	}

}
