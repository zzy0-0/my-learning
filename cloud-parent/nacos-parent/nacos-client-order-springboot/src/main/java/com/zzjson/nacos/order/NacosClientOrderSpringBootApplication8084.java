package com.zzjson.nacos.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
public class NacosClientOrderSpringBootApplication8084 {
	public static void main(String[] args) {
		SpringApplication.run(NacosClientOrderSpringBootApplication8084.class);
	}
}