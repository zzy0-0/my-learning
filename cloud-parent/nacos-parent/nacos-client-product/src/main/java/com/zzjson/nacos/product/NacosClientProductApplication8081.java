package com.zzjson.nacos.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月28日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosClientProductApplication8081 {
	public static void main(String[] args) {
		SpringApplication.run(NacosClientProductApplication8081.class, args);
	}

}