# 初始化

## 1.修改host

### mac环境

```
sudo vim /etc/hosts
```

编辑hosts

```bash
localhost  my-server
```

## 2.导入数据库

执行

`cloud-parent/my-alibaba-dao/init.sql`

导入路径的SQL文件到数据库

## 3.创建nacos

https://nacos.io/zh-cn/docs/quick-start.html

### Docker安装Nacos

```bash
docker run --name nacos-standalone -e MODE=standalone -d -p 8848:8848 nacos/nacos-server:1.1.4
```

Nacos的默认账号密码是

- 账号
    - nacos
- 密码
    - nacos