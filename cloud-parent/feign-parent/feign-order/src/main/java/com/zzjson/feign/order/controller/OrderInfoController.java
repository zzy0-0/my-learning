package com.zzjson.feign.order.controller;

import com.zzjson.common.entity.OrderInfo;
import com.zzjson.common.entity.ProductInfo;
import com.zzjson.common.vo.OrderVo;
import com.zzjson.dao.mapper.OrderInfoMapper;
import com.zzjson.product.feignapi.Params;
import com.zzjson.product.feignapi.ProductCenterFeignApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
public class OrderInfoController {

	@Autowired
	private ProductCenterFeignApi productCenterFeignApi;
	@Autowired
	private OrderInfoMapper orderInfoMapper;

	@RequestMapping("/selectOrderInfoById/{orderNo}")
	public Object selectOrderInfoById(@PathVariable("orderNo") String orderNo) {

		OrderInfo orderInfo = orderInfoMapper.selectById(orderNo);
		if (null == orderInfo) {
			return "根据orderNo:" + orderNo + "查询没有该订单";
		}

		ProductInfo productInfo = productCenterFeignApi.selectProductInfoById(orderNo);

		if (productInfo == null) {
			return "没有对应的商品";
		}

		OrderVo orderVo = new OrderVo();
		orderVo.setOrderNo(orderInfo.getOrderNo());
		orderVo.setUserName(orderInfo.getUserName());
		orderVo.setProductName(productInfo.getProductName());
		orderVo.setProductNum(orderInfo.getProductCount());

		return orderVo;
	}

	@RequestMapping("/getToken")
	public String getToken() {
		return productCenterFeignApi.getToken4Header("123");
	}

	@RequestMapping("/demo/params")
	public String query() {
		Params params = new Params();
		params.setParam1("params11");
		params.setParam2("params22");
		return productCenterFeignApi.demoEndpoint(params);
	}

}