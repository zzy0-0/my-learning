package com.zzjson.feign.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
@EnableFeignClients(basePackages = "com.zzjson.product")
@EnableDiscoveryClient
public class FeignOrderApplication8080 {
	public static void main(String[] args) {
		SpringApplication.run(FeignOrderApplication8080.class);
	}
}