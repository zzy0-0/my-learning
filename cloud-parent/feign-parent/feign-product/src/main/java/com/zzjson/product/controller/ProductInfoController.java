package com.zzjson.product.controller;

import com.zzjson.common.entity.ProductInfo;
import com.zzjson.dao.mapper.ProductInfoMapper;
import com.zzjson.product.feignapi.Params;
import com.zzjson.product.feignapi.ProductCenterFeignApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@Slf4j
public class ProductInfoController implements ProductCenterFeignApi {


	@Autowired
	private ProductInfoMapper productInfoMapper;

	@Override
	//@RequestMapping("/selectProductInfoById/{productNo}")
	public ProductInfo selectProductInfoById(@RequestParam("productNo") String productNo) {

		ProductInfo productInfo = productInfoMapper.selectById(productNo);
		return productInfo;
	}

	@Override
	@RequestMapping("/getToken4Header")
	public String getToken4Header(@RequestHeader("token") String token) {
		log.info("token:{}", token);
		return token;
	}

	//@Override
	@Override
	@GetMapping(path = "/demo/params")
	public String demoEndpoint(Params params) {
		log.info("receive params:{}", params);
		return params.getParam1();
	}
}