package com.zzjson.product.feignapi;

import com.zzjson.common.entity.ProductInfo;
import com.zzjson.product.config.ProductCenterFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */

@FeignClient(name = "product-center", configuration = ProductCenterFeignConfig.class)
//@FeignClient(name = "product-center")
public interface ProductCenterFeignApi {

	/**
	 * 声明式接口,远程调用http://product-center/selectProductInfoById/{productNo}
	 * @param productNo
	 * @return

	 @RequestMapping("/selectProductInfoById/{productNo}") ProductInfo selectProductInfoById(@PathVariable("productNo") String productNo);
	 */


	/**
	 * 修改锲约为Feign的  那么就可以使用默认的注解
	 *
	 * @param productNo
	 * @return
	 */
	//@RequestLine("GET /selectProductInfoById/{productNo}")
	@GetMapping("/selectProductInfoById")
	ProductInfo selectProductInfoById(@RequestParam("productNo") String productNo);


	//@RequestLine("GET /getToken4Header")
	@GetMapping("/getToken4Header")
	String getToken4Header(@RequestHeader("token") String token);


	@GetMapping(path = "/demo/params")
	String demoEndpoint(@SpringQueryMap Params params);

}
