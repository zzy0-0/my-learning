package com.zzjson.product.feignapi;

import lombok.Data;

@Data
public class Params {
	private String param1;
	private String param2;

	// [Getters and setters omitted for brevity]
}