package com.zzjson.product.config;

import com.zzjson.product.handler.MyRequestInterceptor;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class ProductCenterFeignConfig {

/*
    @Bean
    public Logger.Level level() {
        return Logger.Level.FULL;
        //return Logger.Level.BASIC;
    }
*/

	/**
	 * 根据SpringBoot自动装配FeignClientsConfiguration 的FeignClient的契约是SpringMvc
	 *
	 通过修改契约为默认的Feign的锲约，那么就可以使用默认的注解
	 * @return
	 */
	//@Bean
	//public Contract feiContract() {
	//	return new SpringMvcContract();
	//}

	@Bean
	public RequestInterceptor requestInterceptor() {
		return new MyRequestInterceptor();
	}

}