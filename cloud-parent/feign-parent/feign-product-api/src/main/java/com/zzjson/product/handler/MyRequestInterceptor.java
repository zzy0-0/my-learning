package com.zzjson.product.handler;

import feign.RequestInterceptor;
import feign.RequestTemplate;

import java.util.UUID;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MyRequestInterceptor implements RequestInterceptor {
	@Override
	public void apply(RequestTemplate template) {
		template.header("token", UUID.randomUUID().toString());
	}
}