package com.zzjson.ribbon.order.controller;

import com.zzjson.common.entity.OrderInfo;
import com.zzjson.common.entity.ProductInfo;
import com.zzjson.common.vo.OrderVo;
import com.zzjson.dao.mapper.OrderInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@Slf4j
public class OrderInfoController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private OrderInfoMapper orderInfoMapper;

	@RequestMapping("/selectOrderInfoById/{orderNo}")
	public Object selectOrderInfoById(@PathVariable("orderNo") String orderNo) {

		OrderInfo orderInfo = orderInfoMapper.selectById(orderNo);
		if (null == orderInfo) {
			return "根据orderNo:" + orderNo + "查询没有该订单";
		}

		ResponseEntity<ProductInfo> responseEntity = null;
		try {
			responseEntity = restTemplate.getForEntity("http://product-center/selectProductInfoById/" + orderInfo.getProductNo(), ProductInfo.class);

		} catch (Exception e) {
			log.error("调用失败",e);
		}
		if (responseEntity == null) {
			return "调用失败";
		}
		ProductInfo productInfo = responseEntity.getBody();

		if (productInfo == null) {
			return "没有对应的商品";
		}

		OrderVo orderVo = new OrderVo();
		orderVo.setOrderNo(orderInfo.getOrderNo());
		orderVo.setUserName(orderInfo.getUserName());
		orderVo.setProductName(productInfo.getProductName());
		orderVo.setProductNum(orderInfo.getProductCount());

		return orderVo;
	}


}