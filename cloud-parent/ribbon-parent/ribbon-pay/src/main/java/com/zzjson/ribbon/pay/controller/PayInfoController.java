package com.zzjson.ribbon.pay.controller;

import com.zzjson.common.entity.ProductInfo;
import com.zzjson.dao.mapper.ProductInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
public class PayInfoController {
	@Autowired
	private ProductInfoMapper productInfoMapper;

	@RequestMapping("/selectProductInfoById/{productNo}")
	public Object selectProductInfoById(@PathVariable("productNo") String productNo) {

		ProductInfo productInfo = productInfoMapper.selectById(productNo);
		return productInfo;
	}
}