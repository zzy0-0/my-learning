package com.zzjson.ribbon.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
@EnableDiscoveryClient
public class RibbonPayApplication8083 {
	public static void main(String[] args) {
		SpringApplication.run(RibbonPayApplication8083.class);
	}
}