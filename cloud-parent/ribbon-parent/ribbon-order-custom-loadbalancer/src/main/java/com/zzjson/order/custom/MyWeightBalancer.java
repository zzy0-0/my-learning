package com.zzjson.order.custom;

import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.client.naming.core.Balancer;

import java.util.List;

/**
 * <p>****************************************************************************</p>
 * <li>Description : 根据权重选择随机选择一个 </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class MyWeightBalancer extends Balancer {
	public static Instance chooseInstanceByRandomWeight(List<Instance> hosts) {
		return getHostByRandomWeight(hosts);
	}

}