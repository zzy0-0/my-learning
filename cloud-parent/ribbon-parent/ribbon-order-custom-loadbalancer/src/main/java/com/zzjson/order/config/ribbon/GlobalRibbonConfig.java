package com.zzjson.order.config.ribbon;

import com.netflix.loadbalancer.IRule;
import com.zzjson.order.custom.rule.TheSameClusterPriorityWithVersionRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
//@Configuration
public class GlobalRibbonConfig {

	@Bean
	@Primary
	public IRule theSameClusterPriorityRule() {
		//return new MyWeightRule();
		//return new TheSameClusterPriorityRule();
		return new TheSameClusterPriorityWithVersionRule();
	}
}