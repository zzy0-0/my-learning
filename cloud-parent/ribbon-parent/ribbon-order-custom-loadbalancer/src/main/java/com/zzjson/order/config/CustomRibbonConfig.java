package com.zzjson.order.config;

import com.zzjson.order.config.ribbon.GlobalRibbonConfig;
import com.zzjson.order.config.ribbon.PayCenterRibbonConfig;
import com.zzjson.order.config.ribbon.ProductCenterRibbonConfig;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;

/**
 * <p>****************************************************************************</p>
 * <li>Description : ribbon的全局配置 </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年03月03日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Configuration
@RibbonClients(
		defaultConfiguration = GlobalRibbonConfig.class,
		value = {
				@RibbonClient(name = "product-center", configuration = ProductCenterRibbonConfig.class),
				@RibbonClient(name = "pay-center", configuration = PayCenterRibbonConfig.class)
		})
public class CustomRibbonConfig {

}