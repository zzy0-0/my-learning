package com.zzjson.dao.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.zzjson.dao.support.DruidDataSourceProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月28日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@EnableConfigurationProperties(value = DruidDataSourceProperties.class)
@Configuration
@AutoConfigureBefore(MybatisPlusAutoConfiguration.class)
@MapperScan(basePackages = "com.zzjson.dao.mapper")
public class SpringDruidConfig {

	@Autowired
	private DruidDataSourceProperties druidDataSourceProperties;

	@Bean
	@Qualifier
	public DataSource dataSource() throws SQLException {
		System.out.println(druidDataSourceProperties);
		DruidDataSource druidDataSource = new DruidDataSource();
		druidDataSource.setUsername(druidDataSourceProperties.getUsername());
		druidDataSource.setPassword(druidDataSourceProperties.getPassword());
		druidDataSource.setUrl(druidDataSourceProperties.getJdbcUrl());
		druidDataSource.setDriverClassName(druidDataSourceProperties.getDriverClassName());
		druidDataSource.setInitialSize(druidDataSourceProperties.getInitialSize());
		druidDataSource.setMinIdle(druidDataSourceProperties.getMinIdle());
		druidDataSource.setMaxActive(druidDataSourceProperties.getMaxActive());
		druidDataSource.setMaxWait(druidDataSourceProperties.getMaxWait());
		druidDataSourceProperties.setPoolPreparedStatements(druidDataSourceProperties.isPoolPreparedStatements());
		return druidDataSource;
	}
}