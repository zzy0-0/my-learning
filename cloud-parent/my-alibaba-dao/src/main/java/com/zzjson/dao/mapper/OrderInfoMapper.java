package com.zzjson.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzjson.common.entity.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月28日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
}