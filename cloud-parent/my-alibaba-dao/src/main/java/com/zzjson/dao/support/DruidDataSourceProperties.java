package com.zzjson.dao.support;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年02月28日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@ConfigurationProperties(prefix = "spring.datasource.druid")
@Data
public class DruidDataSourceProperties {

	private String username;

	private String password;

	private String jdbcUrl;

	private String driverClassName;

	private Integer initialSize;

	private Integer maxActive;

	private Integer minIdle;

	private long maxWait;

	private boolean poolPreparedStatements;

}