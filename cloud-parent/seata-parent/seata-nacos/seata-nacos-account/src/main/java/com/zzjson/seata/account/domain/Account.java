package com.zzjson.seata.account.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Data
public class Account implements Serializable {
	/**
	 * 主键Id
	 */
	private Integer id;

	/**
	 * 用户Id
	 */
	private Integer userId;

	/**
	 * 余额
	 */
	private BigDecimal balance;
}