package com.zzjson.seata.account.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月13日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public class ThreadLocalExample {

	public static void main(String[] args) throws InterruptedException {

		/*userController.threadLocal.set("2");*/
		//userController.threadLocal.set("11");

		ExecutorService executorService = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 10; i++) {
			int finalI = i;
			executorService.execute(new Thread(() -> {
				UserController userController = new UserController();
				userController.setName("大妈");
				methodA(userController, finalI);
			}, i + ""));
		}
	}

	private static void methodA(UserController userController, int finalI) {
		String name = "nuyi";
		if (finalI == 1) {
			name = "jhgjhg";
			userController.threadLocal.set("123");
		}
		System.out.println(name);
		String s = userController.threadLocal.get();
		System.out.println(Thread.currentThread().getName() + s);
	}

}