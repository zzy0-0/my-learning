package com.zzjson.seata.account;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
@MapperScan("com.zzjson.seata.account.mapper")
@EnableDiscoveryClient
public class SeataAccountApplication8080 {
	public static void main(String[] args) {
		SpringApplication.run(SeataAccountApplication8080.class);
	}
}