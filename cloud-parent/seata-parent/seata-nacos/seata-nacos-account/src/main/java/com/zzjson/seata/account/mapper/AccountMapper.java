package com.zzjson.seata.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzjson.seata.account.domain.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Mapper
public interface AccountMapper extends BaseMapper<Account> {
	/**
	 * 扣减账户余额
	 *
	 * @param userId
	 * @param balance
	 * @return
	 */
	Integer reduceBalance(@Param("userId") Integer userId, @Param("balance") BigDecimal balance);

	Account selectByUserId(@Param("userId") Integer userId);
}