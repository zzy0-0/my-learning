package com.zzjson.seata.account.service;

import java.math.BigDecimal;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public interface IAccountService {
	/**
	 * 扣减账户余额
	 *
	 * @param userId
	 * @param money
	 * @return
	 */
	boolean reduceBalance(Integer userId, BigDecimal money) throws Exception;
}