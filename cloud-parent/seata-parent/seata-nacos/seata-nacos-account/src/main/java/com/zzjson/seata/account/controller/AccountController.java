package com.zzjson.seata.account.controller;

import com.zzjson.nacos.account.api.RemoteAccountService;
import com.zzjson.nacos.account.vo.ResultVo;
import com.zzjson.seata.account.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("account")
public class AccountController implements RemoteAccountService {
	@Autowired
	private IAccountService accountService;

	@Override
	@PostMapping("/reduceBalance")
	@ResponseBody
	public ResultVo reduceBalance(@RequestParam("userId") Integer userId, @RequestParam("money") BigDecimal money) {

		boolean reduceMoneyFlag = false;
		try {
			reduceMoneyFlag = accountService.reduceBalance(userId, money);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		ResultVo resultVo = new ResultVo();
		resultVo.setSuccess(reduceMoneyFlag);
		resultVo.setMsg("扣款成功");
		resultVo.setData("扣款成功");

		return resultVo;
	}
}