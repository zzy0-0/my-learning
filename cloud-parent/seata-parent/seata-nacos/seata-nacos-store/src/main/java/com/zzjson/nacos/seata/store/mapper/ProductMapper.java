package com.zzjson.nacos.seata.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzjson.nacos.seata.store.domain.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

	Integer reduceCount(@Param("productId") Integer productId, @Param("amount") Integer amount);

	Integer reduceCountByBatch(List<Integer> productIds);

	Integer selectCountById(@Param("productId") Integer productId);
}