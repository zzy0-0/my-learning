package com.zzjson.nacos.seata.store;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.zzjson.nacos.seata.store.mapper")
public class SeataNacosStoreApplication8082 {
	public static void main(String[] args) {
		new SpringApplication(SeataNacosStoreApplication8082.class).run(args);
	}
}