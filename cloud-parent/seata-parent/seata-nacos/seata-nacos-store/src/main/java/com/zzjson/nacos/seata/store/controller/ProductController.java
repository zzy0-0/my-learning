package com.zzjson.nacos.seata.store.controller;

import com.zzjson.nacos.seata.store.RemoteStorageService;
import com.zzjson.nacos.seata.store.service.IProductService;
import com.zzjson.nacos.seata.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/product")
public class ProductController implements RemoteStorageService {

	@Autowired
	private IProductService productService;

	/**
	 * 扣减库存
	 */
	@Override
	@RequestMapping("/reduceCount")
	public ResultVo reduceCount(Integer productId, Integer count) {
		boolean reductFlag = productService.reduceCount(count, productId);

		ResultVo resultVo = new ResultVo();
		resultVo.setSuccess(reductFlag);
		resultVo.setMsg("扣除库存成功");
		resultVo.setData("扣除库存成功");

		return resultVo;
	}
}