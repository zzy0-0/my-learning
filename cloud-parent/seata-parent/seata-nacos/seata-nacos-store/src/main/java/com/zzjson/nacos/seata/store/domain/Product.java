package com.zzjson.nacos.seata.store.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Data
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Integer id;

	/**
	 * 商品Id
	 */
	private Integer productId;

	/**
	 * 价格
	 */
	private BigDecimal price;

	/**
	 * 库存数量
	 */
	private Integer count;
}