package com.zzjson.seata.order.vo;

import lombok.Data;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Data
public class ResultVo {
	private boolean success;
	private String msg;
	private Object data;
}