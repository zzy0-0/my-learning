package com.zzjson.seata.order.service;

import com.zzjson.seata.order.domain.Order;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
public interface IOrderService {

	void createOrder(Order order);
}