package com.zzjson.seata.order.controller;

import com.zzjson.seata.order.domain.Order;
import com.zzjson.seata.order.service.IOrderService;
import com.zzjson.seata.order.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private IOrderService orderService;

	/**
	 * 创建订单
	 */
	@GetMapping("/create")
	public ResultVo create(Order order) {
		orderService.createOrder(order);

		ResultVo resultVo = new ResultVo();
		resultVo.setMsg("创建订单成功");
		resultVo.setSuccess(true);
		resultVo.setData("订单ID:"+order.getId());
		return resultVo;
	}
}
