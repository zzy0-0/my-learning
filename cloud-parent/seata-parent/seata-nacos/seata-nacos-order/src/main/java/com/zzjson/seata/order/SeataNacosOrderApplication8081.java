package com.zzjson.seata.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@SpringBootApplication
@EnableFeignClients({"com.zzjson.nacos.seata.store", "com.zzjson.nacos.account.api"})
@EnableDiscoveryClient
@MapperScan("com.zzjson.seata.order.mapper")
public class SeataNacosOrderApplication8081 {
	public static void main(String[] args) {
		new SpringApplication(SeataNacosOrderApplication8081.class).run(args);
	}
}