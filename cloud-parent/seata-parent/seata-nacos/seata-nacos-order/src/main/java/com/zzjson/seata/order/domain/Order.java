package com.zzjson.seata.order.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Data
public class Order implements Serializable {
	/**
	 * 主键Id
	 */
	private Integer id;

	/**
	 * 用户Id
	 */
	private Integer userId;

	/**
	 * 付款金额
	 */
	private BigDecimal payMoney;

	/**
	 * 商品Id
	 */
	private Integer productId;

	/**
	 * 状态 0下单 1完成
	 */
	private Integer status;

	/**
	 * 商品数量
	 */
	private Integer count;
}