package com.zzjson.seata.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzjson.seata.order.domain.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月12日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

	void saveOrder(Order order);

	void updateOrderStatusById(Integer id, int i);
}