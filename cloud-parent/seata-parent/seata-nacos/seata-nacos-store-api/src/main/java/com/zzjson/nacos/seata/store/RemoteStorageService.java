package com.zzjson.nacos.seata.store;

import com.zzjson.nacos.seata.vo.ResultVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>****************************************************************************</p>
 * <li>Description : TODO </li>
 * <li>Version     : 1.0.0</li>
 * <li>Creation    : 2021年04月14日</li>
 * <li>@author     : zzy0_0</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@FeignClient(value = "nacos-seata-storage-server")
public interface RemoteStorageService {

	/**
	 * 扣减库存
	 */
	@GetMapping(value = "/product/reduceCount")
	ResultVo reduceCount(@RequestParam("productId") Integer productId, @RequestParam("count") Integer count);

}